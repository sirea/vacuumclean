# VacuumClean
project for controlling (deebot) vacuum cleaners, currently Ecovacs Deebot Ozmo 930 is supported

## Quick Start Guide
### Requirements
* clone repository
* configure DNS server (e.g. using dnsmasq)
1. address=/ecouser.net/server ip
2. address=/ecovacs.com/server ip
3. address=/ecovacs.net/server ip
4. address=/ecorobot.net/server ip
* set IP of robot server point to your local ip (nothing to change if robot and xmpp server run on the same machine)
* start specific RobotServer, e.g. DeebotRobotServer
* start specific XMPP server, e.g. DeebotXMPPServer
* VacuumCleanRunner encapsulates the robot and the XMPP server

### WiFi configuration
For connection to the robot, it first has to establish a network connection. After resetting the robot, it provides an access point called ECOVACS_XXXX. You have to connect to this access point to send network credentials to the robot. The following string needs to be sent to the robot:

`@{"td":"scpa","ssid":"$SSID","passphrase":"$PASSPHRASE","encrypt":"yes","append_info":"$APPEND_INFO","slk_msg":"$CONFIG_SETTINGS"}`

### Executable docker images
Executable docker images for the project are available at docker hub:
 * [server image](https://hub.docker.com/r/caterdev/deebot-runner)
 * [client image](https://hub.docker.com/r/caterdev/deebot-client)

### Controlling the robot
Currently, we have a prototypical client implementation (deebot-client). This standalone application starts an embedded kto server and waits for inputs by calling specific URLs (usually, the server is started at http://localhost:8080). You have to adapt `setHost` in `Deebot930ConnectionManager.kt` to your local IP address.

The following actions are supported:
* /battery: prints the battery power
* /chargeType: prints the charge type
* /getChargerPos: prints the coordinates of the charger
* /getPos: prints the coordinates of the bot

The client does not provide any authentication or authorisation at the moment. Instead, it connects to the bot using credentials username23/password/resource. When the client connects the first time to an existing robot it will usually get a permission denied error. If this happens, a new user `username23` is created and provided with necessary permissions (userman, setting, clean).

## Certificate Generation
### Create Root CA
1. create csrconfig-root-ca.txt
```
[ req ]
default_md = sha256
prompt = no
req_extensions = req_ext
distinguished_name = req_distinguished_name
[ req_distinguished_name ]
commonName = VacuumClean
organizationName = caterdev
[ req_ext ]
keyUsage=critical,keyCertSign,cRLSign
basicConstraints=critical,CA:true,pathlen:1
```

2. create certconfig-root-ca.txt
```
[ req ]
default_md = sha256
prompt = no
req_extensions = req_ext
distinguished_name = req_distinguished_name
[ req_distinguished_name ]
commonName = VacuumClean
organizationName = caterdev
[ req_ext ]
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid:always,issuer
keyUsage=critical,keyCertSign,cRLSign
basicConstraints=critical,CA:true,pathlen:1
```

3. generate RSA private key
```
openssl genpkey -outform PEM -algorithm RSA -pkeyopt rsa_keygen_bits:2048 -out priv.key
```

4. create CSR
```
openssl req -new -nodes -key priv.key -config csrconfig-root-ca.txt -out cert.csr
```

5. self-sign CSR
```
openssl req -x509 -nodes -in cert.csr -days 3650 -key priv.key -config certconfig-root-ca.txt -extensions req_ext -out ca.crt
```

### Create Server Certificate
1. create csrconfig-server-cert.txt
```
[ req ]
default_md = sha256
prompt = no
req_extensions = req_ext
distinguished_name = req_distinguished_name
[ req_distinguished_name ]
commonName = VacuumCleaner
organizationName = caterdev
[ req_ext ]
keyUsage=critical,digitalSignature,keyEncipherment
extendedKeyUsage=serverAuth,clientAuth
basicConstraints=critical,CA:false
subjectAltName = @alt_names
[ alt_names ]
DNS.0 = ecovacs.com
DNS.1 = *.ecovacs.com
DNS.2 = ecouser.net
DNS.3 = *.ecouser.net
DNS.4 = ecovacs.net
DNS.5 = *.ecovacs.net
```

2. create certconfig-server-cert.txt
```
[ req ]
default_md = sha256
prompt = no
req_extensions = req_ext
distinguished_name = req_distinguished_name
[ req_distinguished_name ]
commonName = VacuumCleaner
organizationName = caterdev
[ req_ext ]
subjectKeyIdentifier = hash
authorityKeyIdentifier = keyid:always,issuer
keyUsage=critical,digitalSignature,keyEncipherment
extendedKeyUsage=serverAuth,clientAuth
basicConstraints=critical,CA:false
subjectAltName = @alt_names
[ alt_names ]
DNS.0 = ecovacs.com
DNS.1 = *.ecovacs.com
DNS.2 = ecouser.net
DNS.3 = *.ecouser.net
DNS.4 = ecovacs.net
DNS.5 = *.ecovacs.net
```

3. generate RSA private key
```
openssl genpkey -outform PEM -algorithm RSA -pkeyopt rsa_keygen_bits:2048 -out priv.key
```

4. create CSR
```
openssl req -new -nodes -key priv.key -config csrconfig-server-cert.txt -out cert.csr
```

5. sign CSR with Root CA cert
```
openssl x509 -req -in cert.csr -days 3650 -CA ca.crt -CAkey priv.key -extfile certconfig-server-cert.txt -extensions req_ext -CAserial /tmp/tmp-10593TSH1OlVSxC7C -CAcreateserial -out cert.crt
```

### Generierung mit keytool
```  
keytool -genkey -alias vacuumcleaner -keystore vacuumcleaner.keystore -keyalg RSA -validity 9999 -dname "CN=caterdev" -ext san=dns:ecovacs.com,dns:ecouser.net,dns:ecovacs.net
``` 

TODO: key importieren als trusted key
`keytool -import -file vacuumcleaner.cer -alias vacuumcleaner -cacerts