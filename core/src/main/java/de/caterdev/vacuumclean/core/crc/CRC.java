/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vacuumclean.core.crc;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

// implementation from https://github.com/rdfhdt/hdt-java/tree/master/hdt-java-core/src/main/java/org/rdfhdt/hdt/util/crc
public interface CRC extends Comparable<CRC> {
    /**
     * Update this CRC with the content of the buffer, from offset, using length bytes.
     */
    void update(byte[] buffer, int offset, int length);

    /**
     * Updates this CRC with the content of the buffer
     *
     * @param buffer
     */
    void update(byte[] buffer);

    /**
     * Update the CRC with the specified byte
     */
    void update(byte data);

    /**
     * Write this CRC to an Output Stream
     */
    void writeCRC(OutputStream out) throws IOException;

    /**
     * Read CRC from InputStream and compare it to this.
     *
     * @param in InputStream
     * @return true if the checksum is the same, false if checksum error.
     * @throws IOException
     */
    boolean readAndCheck(InputStream in) throws IOException;

    /**
     * Get checksum value.
     *
     * @return
     */
    long getValue();

    /**
     * Reset the checksum to the initial value.
     */
    void reset();
}
