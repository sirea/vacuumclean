/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vacuumclean.server;

import de.caterdev.vacuumclean.log.XMPPMesageLog;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import io.netty.util.ReferenceCountUtil;
import lombok.Getter;
import lombok.extern.java.Log;
import org.jivesoftware.smack.packet.Stanza;
import org.jivesoftware.smack.packet.StartTls;
import org.jivesoftware.smack.sasl.packet.SaslStreamElements;
import org.jivesoftware.smack.util.PacketParserUtils;
import org.jxmpp.jid.impl.JidCreate;
import org.xmlpull.v1.XmlPullParser;

import java.nio.charset.Charset;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Log
public class NettyXMPPServerHandler<T extends NettyXMPPClient> extends ChannelInboundHandlerAdapter {
    public static final String LOG_MESSAGE_INCOMING_CONNECTION = "Incoming connection from %s\n";
    public static final String LOG_MESSAGE_CLOSING_CONNETION = "Closing connection with %s\n";
    public static final String LOG_MESSAGE_INCOMING = "read (fr: %s, ts: %s)\t %s\n";
    public static final String LOG_MESSAGE_STANZA_CLASS = "stanza class: %s\n";

    @Getter
    private final T client;
    @Getter
    private final NettyXMPPServer<T> server;
    private boolean encrypted = false;


    public NettyXMPPServerHandler(T client, NettyXMPPServer<T> server) {
        this.client = client;
        this.client.setHandler(this);

        this.server = server;
    }

    @Override
    public void channelRead(ChannelHandlerContext ctx, Object msg) throws Exception {
        String message = ((ByteBuf) msg).toString(Charset.defaultCharset());
        XMPPMesageLog.logRead(message);

        log.info(String.format(LOG_MESSAGE_INCOMING, client, LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME), message));

        try {
            XmlPullParser parser = PacketParserUtils.getParserFor(message);


            boolean initEncryption = false;
            switch (parser.getName()) {
                case "stream": {
                    if (!client.isAuthenticated()) {
                        client.handleInitStreamUnauthorised(parser, encrypted);
                    } else {
                        client.handleStreamFeatures();
                    }
                    break;
                }
                case StartTls.ELEMENT: {
                    client.send("<proceed xmlns='urn:ietf:params:xml:ns:xmpp-tls'/>");
                    initEncryption = true;
                    if (!encrypted && initEncryption) {
                        ctx.channel().pipeline().addBefore("xmpp", "tls", NettySSLHandler.getSSLHandler());
                        encrypted = true;
                    }
                    break;
                }
                case SaslStreamElements.AuthMechanism.ELEMENT: {
                    client.handleAuthorisation(parser);
                    break;
                }
                default: {
                    Stanza stanza = PacketParserUtils.parseStanza(message);
                    log.fine(String.format(LOG_MESSAGE_STANZA_CLASS, stanza.getClass()));

                    if (null != stanza.getTo() && !server.getAppServerJids().contains(stanza.getTo()) && !server.getBotServerJids().contains(stanza.getTo())) {
                        T receiver = server.getClient(stanza.getTo());

                        if (null != receiver) {
                            if (null == stanza.getFrom()) {
                                // TODO: prüfen: wir gehen davon aus, dass nur der Robot mit leerem from sendet und fügen das deshalb hier ein - from-Angabe ist für die Weiterverarbeitung in der App notwendig
                                stanza.setFrom(JidCreate.from(server.getRobotClient().getClientJid()));
                            }
                            receiver.forward(stanza);
                        } else {
                            // TODO mal mit reinnehmen, wenn es an den admin-user geht
                            if (stanza.getTo().asUnescapedString().equals("dcjl2q7ac46b2641@ecouser.net")) {
                                stanza.setTo(server.getUserClients().get(0).getClientJid());
                                stanza.setFrom(JidCreate.from(server.getRobotClient().getClientJid()));
                                server.getUserClients().get(0).forward(stanza);
                            } else {
                                System.err.printf("did not find client with jid %s\n", stanza.getTo());
                                client.handleStanza(stanza, parser, server);
                            }
                        }
                    } else {
                        client.handleStanza(stanza, parser, server);
                    }
                    break;
                }
            }
        } catch (final Exception e) {
            e.printStackTrace();
        } finally {
            ReferenceCountUtil.release(msg);
        }
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        log.info(String.format(LOG_MESSAGE_INCOMING_CONNECTION, ctx.channel().remoteAddress()));
        client.setChannelHandlerContext(ctx);

        super.channelActive(ctx);
    }


    @Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        log.info(String.format(LOG_MESSAGE_CLOSING_CONNETION, ctx.channel().remoteAddress()));
        server.getClients().remove(client);

        super.channelInactive(ctx);
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) {
        cause.printStackTrace();
        ctx.close();
    }
}
