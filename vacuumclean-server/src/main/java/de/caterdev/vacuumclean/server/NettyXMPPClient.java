/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vacuumclean.server;

import de.caterdev.vacuumclean.core.DeviceType;
import de.caterdev.vacuumclean.core.MessageType;
import de.caterdev.vacuumclean.log.XMPPMesageLog;
import de.caterdev.vacuumclean.packets.RosterResult;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.java.Log;
import org.jivesoftware.smack.packet.*;
import org.jivesoftware.smack.packet.id.StanzaIdUtil;
import org.jivesoftware.smack.roster.packet.RosterPacket;
import org.jivesoftware.smack.sasl.SASLMechanism;
import org.jivesoftware.smackx.disco.packet.DiscoverInfo;
import org.jivesoftware.smackx.ping.packet.Ping;
import org.jxmpp.jid.EntityFullJid;
import org.jxmpp.jid.impl.JidCreate;
import org.jxmpp.jid.parts.Resourcepart;
import org.xmlpull.v1.XmlPullParser;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Log
public abstract class NettyXMPPClient {
    public static final String LOG_MESSAGE_SEND = "%s: (to: %s, ts: %s)\t %s\n";


    @Getter
    private final String botServerDomain;
    @Getter
    private final String appServerDomain;

    @Getter
    @Setter
    private boolean authenticated = false;
    @Getter
    @Setter
    private String clientName;
    @Getter
    @Setter
    private Resourcepart resourceBind;
    @Getter
    @Setter
    private EntityFullJid clientJid;
    private DeviceType deviceType = DeviceType.Unknown;
    @Getter
    @Setter
    private ChannelHandlerContext channelHandlerContext;
    @Getter
    @Setter
    private NettyXMPPServerHandler handler;

    public NettyXMPPClient(String botServerDomain, String appServerDomain) {
        this.botServerDomain = botServerDomain;
        this.appServerDomain = appServerDomain;

        this.deviceType = DeviceType.Unknown;
    }

    public boolean isRobot() {
        return deviceType == DeviceType.Robot;
    }

    protected void handleStreamFeatures() {
        String command = new StreamOpen("to", getBotServerDomain(), StanzaIdUtil.newStanzaId()).toXML(StreamOpen.CLIENT_NAMESPACE).toString() +
                "<stream:features>" +
                Bind.Feature.INSTANCE.toXML(null) +
                new Session.Feature(true).toXML(null) +
                "</stream:features>";

        send(command);
    }

    protected void handleResourceBind(Bind stanza) throws Exception {
        setResourceBindAndJid(stanza.getResource());

        Bind result = Bind.newResult(getClientJid());
        result.setType(IQ.Type.result);
        result.setStanzaId(stanza.getStanzaId());

        send(result);
    }

    protected void handleResultExpected(IQ request) {
        send(IQ.createResultIQ(request));
    }


    protected abstract <U extends NettyXMPPClient> void handleIQ(IQ iq, XmlPullParser parser, NettyXMPPServer<U> server) throws Exception;

    protected final <U extends NettyXMPPClient> void handleStanza(Stanza stanza, XmlPullParser parser, NettyXMPPServer<U> server) throws Exception {
        if (stanza instanceof Presence) {
            handlePresence((Presence) stanza);
        } else if (stanza instanceof Bind) {
            handleResourceBind((Bind) stanza);
        } else if (stanza instanceof Ping) {
            handlePing((Ping) stanza);
        } else if (stanza instanceof RosterPacket) {
            handleRoster((RosterPacket) stanza);
        } else if (stanza instanceof DiscoverInfo) {
            handleDisco((DiscoverInfo) stanza);
        } else if (stanza instanceof IQ) {
            handleIQ((IQ) stanza, parser, server);
        }
    }

    protected void handlePing(Ping ping) {
        send(ping.getPong());
    }

    protected void handleRoster(RosterPacket rosterPacket) {
        send(new RosterResult(handler.getServer().getRobotClients(), rosterPacket));
    }

    protected void handleDisco(DiscoverInfo discoPacket) {
        send(DiscoverInfo.createErrorResponse(discoPacket, StanzaError.Condition.feature_not_implemented));
    }

    protected abstract void handlePresence(Presence presence) throws Exception;

    public void setResourceBindAndJid(Resourcepart resourceBind) throws Exception {
        this.resourceBind = resourceBind;

        if (isRobot()) {
            setClientJid(JidCreate.entityFullFrom(clientName + "@" + botServerDomain + "/" + resourceBind));
        } else {
            setClientJid(JidCreate.entityFullFrom(clientName + "@" + appServerDomain + "/" + resourceBind));
        }
    }

    public void send(String message, MessageType type) {
        if (type != MessageType.fwd) {
            XMPPMesageLog.logWrite(message);
        }
        log.info(String.format(LOG_MESSAGE_SEND, type, this, LocalDateTime.now().format(DateTimeFormatter.ISO_LOCAL_DATE_TIME), message));

        ByteBuf response = channelHandlerContext.alloc().buffer(message.getBytes().length);
        response.writeBytes(message.getBytes());
        channelHandlerContext.writeAndFlush(response);//.sync();
    }

    public void forward(String message) {
        send(message, MessageType.fwd);
    }

    public void forward(Stanza stanza) {
        forward(stanza.toXML(null).toString());
    }

    public void send(String message) {
        send(message, MessageType.rpl);
    }

    public void send(Stanza stanza) {
        send(stanza.toXML(null).toString());
    }

    @Override
    public String toString() {
        StringBuilder result = new StringBuilder(channelHandlerContext.channel().remoteAddress().toString());

        if (null != clientJid) {
            result.append("[").append(clientJid).append("]");
        }

        result.append("[type=").append(deviceType).append("]");

        return result.toString();
    }

    protected abstract void handleAuthorisation(XmlPullParser parser) throws Exception;

    public void handleInitStreamUnauthorised(XmlPullParser parser, boolean encrypted) {
        StreamOpen streamOpen = new StreamOpen("to");
        Mechanisms authMechanisms = new Mechanisms(SASLMechanism.PLAIN);
        StartTls startTls = new StartTls(true);

        StringBuilder command = new StringBuilder(streamOpen.toXML(StreamOpen.CLIENT_NAMESPACE).toString());
        command.append("<stream:features xmlns='jabber:client' xmlns:stream='http://etherx.jabber.org/streams'>");

        if (!encrypted) {
            command.append(startTls.toXML(null).toString());
        } else {
            command.append(authMechanisms.toXML(null).toString());
        }

        command.append("</stream:features>");

        if (null != parser.getAttributeValue(null, "to") && parser.getAttributeValue(null, "to").equals(botServerDomain)) {
            deviceType = DeviceType.Robot;
        } else {
            deviceType = DeviceType.Client;
        }

        send(command.toString());
    }
}
