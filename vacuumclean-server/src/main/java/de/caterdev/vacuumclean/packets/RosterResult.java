/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vacuumclean.packets;

import de.caterdev.vacuumclean.server.NettyXMPPClient;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.roster.packet.RosterPacket;

import java.util.Collection;

public class RosterResult extends IQ {
    public static final String ELEMENT_ITEM = "item";
    public static final String ELEMENT_GROUP = "group";
    public static final String ATTRIBUTE_JID = "jid";
    public static final String ATTRIBUTE_NAME = "name";
    public static final String ATTRIBUTE_SUBSCRIPTION = "subscription";
    public static final String VALUE_BOTH = "both";

    private final Collection<NettyXMPPClient> clients;

    public RosterResult(Collection<NettyXMPPClient> clients, RosterPacket request) {
        super(QUERY_ELEMENT, RosterPacket.NAMESPACE);

        this.clients = clients;

        initializeAsResultFor(request);
    }

    @Override
    protected IQChildElementXmlStringBuilder getIQChildElementBuilder(IQChildElementXmlStringBuilder xml) {
        xml.rightAngleBracket();
        for (NettyXMPPClient client : clients) {
            xml.halfOpenElement(ELEMENT_ITEM);
            xml.attribute(ATTRIBUTE_JID, client.getClientJid());
            xml.attribute(ATTRIBUTE_NAME, client.getClientName());
            xml.attribute(ATTRIBUTE_SUBSCRIPTION, VALUE_BOTH);
            xml.rightAngleBracket();
            xml.element(ELEMENT_GROUP, client.getResourceBind().toString());
            xml.closeElement(ELEMENT_ITEM);
        }

        return xml;
    }
}
