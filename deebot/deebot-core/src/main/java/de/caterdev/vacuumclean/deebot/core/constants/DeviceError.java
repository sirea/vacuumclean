/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vacuumclean.deebot.core.constants;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum DeviceError {
    NoError(100),
    BatteryLow(101),
    HostHang(102),
    WheelAbnormal(103),
    DownSensorAbnormal(104),
    Stuck(105),
    SideBrushExhausted(106),
    DustCaseExhausted(107),
    SideBrushAbnormal(108),
    RollAbnormal(109),
    NoDustBox(110),
    BumpAbnormal(111),
    LDS(112),
    MainBrushExhausted(113),
    DustcaseFilled(114),
    BatteryErrpr(115),
    ForwardlookingError(116),
    GyroscopeError(117),
    StrainerBlock(118),
    FanError(119),
    WaterboxError(120),
    PlateJammed(121),
    AirfilterUninstall(201),
    UltrasonicComponentAbnormal(202),
    SmallWheelError(203),
    WheelHang(204),
    ION_STERILIZE_EXHAUSTED(205),
    ION_STERILIZE_ABNORMAL(206),
    ION_STERILIZE_FAULT(207);

    private final int errno;

}
