/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vacuumclean.deebot.core.map;

import lombok.Data;
import lombok.ToString;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.zip.CRC32;

@Data
public class MapInfo {
    public static final byte NODATA = 0;
    public static final byte FLOOR = 1;
    public static final byte WALL = 2;
    public static final byte CARPET = 3;

    private final int columnGrid;
    private final int columnPiece;
    private final int gridSize;
    private final int height;
    private final int pieceCount;
    private final int pixelWidth;
    private final int rowGrid;
    private final int rowPiece;
    private final int width;
    @ToString.Exclude
    private final byte[][] buffer;
    private Position boxBottomRight;
    private Position boxTopLeft;
    private int mapId = -1;
    private MapType mapType;

    public MapInfo(MapModel mapModel) {
        this.mapId = mapModel.getMapId();
        this.pixelWidth = mapModel.getPixelWidth();
        this.boxTopLeft = mapModel.getBoxTopLeft();
        this.boxBottomRight = mapModel.getBoxBottomRight();

        this.rowGrid = mapModel.getRowGrid();
        this.rowPiece = mapModel.getRowPiece();

        this.columnGrid = mapModel.getColumnGrid();
        this.columnPiece = mapModel.getColumnPiece();

        this.height = rowGrid * rowPiece;
        this.width = columnGrid * columnPiece;

        this.pieceCount = rowPiece * columnPiece;
        this.gridSize = rowGrid * columnGrid;

        this.buffer = new byte[height][width];
    }

    /**
     * Reads persistent binary map data from the given file.
     *
     * @param filename the name of the file containing the map data
     * @param mapModel the map model describing the map data
     * @return the map data
     * @throws IOException
     */
    public static MapInfo readMapData(String filename, MapModel mapModel) throws IOException {
        FileInputStream fis = new FileInputStream(filename);

        byte[] mapData = new byte[mapModel.getDataSize()];
        fis.read(mapData);
        fis.close();

        MapInfo mapInfo = new MapInfo(mapModel);
        mapInfo.updateMapBuffer(mapData);

        return mapInfo;
    }

    /**
     * Returns this map data as a single byte stream
     *
     * @return single byte stream representing this map
     */
    public byte[] getBufferAsOne() {
        byte[] buffer = new byte[this.width * this.height];
        for (int i = 0; i < buffer.length; i++) {
            buffer[i] = this.buffer[i / this.width][i % this.width];
        }
        return buffer;
    }

    /**
     * Updates this map data with the given byte stream.
     *
     * @param buffer the byte stream representing the map data
     */
    public void updateMapBuffer(byte[] buffer) {
        for (int i = 0; i < buffer.length; i++) {
            this.buffer[i / this.width][i % this.width] = buffer[i];
        }
    }

    /**
     * Updates the map with the values contained in the given map piece.
     *
     * @param mapPiece the map piece with respective data
     */
    public void updateMapBuffer(MapPiece mapPiece) {
        final int pieceId = mapPiece.getPieceId();
        final byte[] pieceData = mapPiece.getData();

        int rowStart = pieceId / rowPiece;
        int columnStart = pieceId % columnPiece;


        for (int row = 0; row < rowGrid; row++) {
            for (int column = 0; column < columnGrid; column++) {
                int bufferRow = row + rowStart * rowGrid;
                int bufferColumn = column + columnStart * columnGrid;
                int pieceDataPosition = rowGrid * row + column;

                this.buffer[bufferRow][bufferColumn] = pieceData[pieceDataPosition];
            }
        }
    }

    /**
     * Returns the {@link CRC32} checksum of the specified piece.
     *
     * @param pieceId the id of the piece
     * @return the {@link CRC32} checksum of the given piece
     */
    public long getChecksum(int pieceId) {
        if (pieceId >= this.pieceCount) {
            throw new RuntimeException("Piece Id " + pieceId + " greater than piececount " + pieceCount);
        }

        byte[] data = new byte[this.gridSize];

        int rowStart = pieceId / this.rowPiece;
        int columnStart = pieceId % this.columnPiece;

        for (int row = 0; row < this.rowGrid; row++) {
            for (int column = 0; column < this.columnGrid; column++) {
                int bufferRow = row + rowStart * this.rowGrid;
                int bufferColumn = column + columnStart * this.columnGrid;
                int dataPosition = rowGrid * row + column;

                data[dataPosition] = this.buffer[bufferRow][bufferColumn];
            }
        }

        CRC32 checksum = new CRC32();
        checksum.update(data);
        return checksum.getValue();
    }

    /**
     * Writes the binary map data of this map to the given file.
     *
     * @param filename the file to write to
     * @throws IOException
     */
    public void writeMapData(String filename) throws IOException {
        FileOutputStream fos = new FileOutputStream(filename);
        fos.write(getBufferAsOne());
        fos.flush();
        fos.close();
    }

    /**
     * Writes the map data to an HTML file with a HTML5 canvas.
     *
     * @param filename the filename of the HTML file
     * @throws IOException
     */
    public void writeUnscaledHTML(String filename) throws IOException {
        System.out.println("dimensions: " + width + " x " + height);
        FileWriter writer = new FileWriter(filename);
        writer.write("<!DOCTYPE html><html><head><title>Simple Canvas Example</title><style>canvas {border: 3px #CCC solid;}  </style></head><body><div id=\"container\"><canvas id=\"myCanvas\" height='" + (4000) + "' width='" + (4000) + "'></canvas></div><script>var mainCanvas = document.querySelector(\"#myCanvas\");var mainContext = mainCanvas.getContext(\"2d\");var canvasWidth = mainCanvas.width;var canvasHeight = mainCanvas.height;function drawCircle() {mainContext.clearRect(0, 0, canvasWidth, canvasHeight);\n");
        byte type = -1;
        for (int i = 0; i < buffer.length; i++) {
            for (int j = 0; j < buffer[i].length; j++) {
                byte mapBuffer = buffer[i][j];

                if ((mapBuffer == MapInfo.NODATA) && (type != MapInfo.NODATA)) {
                    //writer.write("mainContext.fillStyle='FFFF0F';\n");
                    //type=NODATA;
                    continue;
                }

                if (mapBuffer == MapInfo.WALL && type != MapInfo.WALL) {
                    writer.write("mainContext.fillStyle = '#AAAAAA';\n");
                    type = MapInfo.WALL;
                } else if (mapBuffer == MapInfo.FLOOR && type != MapInfo.FLOOR) {
                    writer.write("mainContext.fillStyle = '#FFFFAA';\n");
                    type = MapInfo.FLOOR;
                } else if (mapBuffer == MapInfo.CARPET && type != MapInfo.CARPET) {
                    writer.write("mainContext.fillStyle = '#CCCCCC';\n");
                    type = MapInfo.CARPET;
                } else if (mapBuffer > CARPET) {
                    System.out.println("unknown type" + mapBuffer);
                }

                writer.write("mainContext.fillRect(" + i * 5 + "," + j * 5 + "," + 1 * 5 + "," + 1 * 5 + ");\n");

            }
        }
        writer.write("mainContext.closePath();}drawCircle();</script></body></html>");
        writer.close();
    }

    public String getUnscaledHTMLCanvas() {
        StringBuilder builder = new StringBuilder();
        byte type = -1;
        for (int i = 0; i < buffer.length; i++) {
            for (int j = 0; j < buffer[i].length; j++) {
                byte mapBuffer = buffer[i][j];

                if ((mapBuffer == MapInfo.NODATA) && (type != MapInfo.NODATA)) {
                    //writer.write("mainContext.fillStyle='FFFF0F';\n");
                    //type=NODATA;
                    continue;
                }

                if (mapBuffer == MapInfo.WALL && type != MapInfo.WALL) {
                    builder.append("mainContext.fillStyle = '#AAAAAA';\n");
                    type = MapInfo.WALL;
                } else if (mapBuffer == MapInfo.FLOOR && type != MapInfo.FLOOR) {
                    builder.append("mainContext.fillStyle = '#FFFFAA';\n");
                    type = MapInfo.FLOOR;
                } else if (mapBuffer == MapInfo.CARPET && type != MapInfo.CARPET) {
                    builder.append("mainContext.fillStyle = '#CCCCCC';\n");
                    type = MapInfo.CARPET;
                } else if (mapBuffer > CARPET) {
                    System.out.println("unknown type" + mapBuffer);
                }

                builder.append("mainContext.fillRect(" + i + "," + j + "," + 1 + "," + 1 + ");\n");

            }
        }

        return builder.toString();
    }
}
