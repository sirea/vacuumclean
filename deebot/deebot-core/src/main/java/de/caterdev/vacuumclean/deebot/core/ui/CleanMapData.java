/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vacuumclean.deebot.core.ui;

import de.caterdev.vacuumclean.deebot.core.map.MapDetail;
import de.caterdev.vacuumclean.deebot.core.map.MapInfo;
import de.caterdev.vacuumclean.deebot.core.map.MapSetType;
import de.caterdev.vacuumclean.deebot.core.map.Position;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.ToString;
import lombok.extern.java.Log;

import java.io.FileWriter;
import java.util.*;

@Data
@Log
public class CleanMapData {
    private int minX = Integer.MAX_VALUE;
    private int minY = Integer.MAX_VALUE;
    private int maxX = Integer.MIN_VALUE;
    private int maxY = Integer.MIN_VALUE;

    @ToString.Exclude
    private MapInfo mapInfo;
    @ToString.Exclude
    private Map<MapSetType, List<MapDetail>> mapDetails = new HashMap<>();

    private Map<MapSetType, Integer> currentMapSetId = new HashMap<>();

    private float windowWidth = 0;
    private float windowHeight = 0;

    private float viewWidth = 0;
    private float viewHeight = 0;

    private float scale = 0;
    private float preScale = 0;

    private float x = 0;
    private float y = 0;

    private Position botPosition = new Position(0, 0, 0);
    private Position chargerPosition = new Position(0, 0, 0);

    public void addMapDetail(MapDetail mapDetail) {
        if (null == mapDetails.get(mapDetail.getMapSetType())) {
            mapDetails.put(mapDetail.getMapSetType(), new ArrayList<>());
        }

        mapDetails.get(mapDetail.getMapSetType()).add(mapDetail);
    }

    public float getPhoneX(final float originalX) {
        //return this.viewWidth / 2.0f + (n - this.x) * this.scale;
        return viewWidth / 2 + (originalX - x) * scale;
    }

    public float getPhoneY(final float originalY) {
        return viewHeight / 2 + (originalY - y) * scale;
        //return this.viewHeight / 2.0f - (n - this.y) * this.scale;
    }

    public void updateScale(float width, float height) {
        this.windowWidth = width;
        this.windowHeight = height;
        this.viewWidth = width;
        this.viewHeight = height;

        for (int x = 0; x < mapInfo.getWidth(); x++) {
            for (int y = 0; y < mapInfo.getHeight(); y++) {
                if (0 != mapInfo.getBuffer()[x][y]) {
                    this.minX = Math.min(this.minX, x);
                    this.minY = Math.min(this.minY, y);
                    this.maxX = Math.max(this.maxX, x);
                    this.maxY = Math.max(this.maxY, y);
                }
            }
        }

        float mapColumnPixel = mapInfo.getColumnGrid() * mapInfo.getPixelWidth();
        float mapRowPixel = mapInfo.getRowGrid() * mapInfo.getPixelWidth();

        float columnScale = (float) (maxX - minX) * mapInfo.getPixelWidth();
        float rowScale = (float) (maxY - minY) * mapInfo.getPixelWidth();
        if (columnScale < mapColumnPixel) {
            columnScale = mapColumnPixel;
        }
        if (rowScale < mapRowPixel) {
            rowScale = mapRowPixel;
        }

        this.scale = Math.min(this.viewHeight / rowScale, this.viewWidth / columnScale);
        this.preScale = this.scale * 50.0F + 0.5F; // TODO: Warum 50 und 0.5? -> 50 eventuell wieder pixelWidth


        // TODO: change to:
        /*
        float f1 = ((getMaxX() - mapInfo.getWidth() / 2) * mapInfo.getPixelWidth() + (getMinX() - mapInfo.getWidth() / 2) * mapInfo.getPixelWidth());
        float f2 = ((getMaxY() - mapInfo.getHeight() / 2) * mapInfo.getPixelWidth() + (getMinY() - mapInfo.getHeight() / 2) * mapInfo.getPixelWidth());
         */
        float f1 = ((getMaxX() - 400) * 50 + (getMinX() - 400) * 50); // TODO: Warum 400, 50, 2?
        float f2 = ((getMaxY() - 400) * 50 + (getMinY() - 400) * 50);
        setX(f1 / 2.0F);
        setY(f2 / 2.0F);
    }

    public float gridPositionXToMapPositionX(int gridPositionX) {
        return viewWidth / 2 + ((gridPositionX - mapInfo.getWidth() / 2) * mapInfo.getPixelWidth() - x) * scale;
    }

    public float gridPositionYtoMapPositionY(int gridPositionY) {
        return viewHeight / 2 + ((gridPositionY - mapInfo.getHeight() / 2) * mapInfo.getPixelWidth() - y) * scale;
    }

    public List<List<Rect>> update() {
        List<Rect> floor = new ArrayList<>();
        List<Rect> wall = new ArrayList<>();
        List<Rect> carpet = new ArrayList<>();

        for (int x = minX; x <= maxX; x++) {
            for (int y = minY; y <= maxY; y++) {
                final float mapX = gridPositionXToMapPositionX(x);
                final float mapY = gridPositionYtoMapPositionY(y);

                switch (mapInfo.getBuffer()[x][y]) {
                    case MapInfo.NODATA: {
                        break;
                    }
                    case MapInfo.FLOOR: {
                        floor.add(new Rect(mapX, mapY, mapX + getPreScale(), mapY + getPreScale()));
                        break;
                    }
                    case MapInfo.WALL: {
                        wall.add(new Rect(mapX, mapY, mapX + getPreScale(), mapY + getPreScale()));
                        break;
                    }
                    case MapInfo.CARPET: {
                        carpet.add(new Rect(mapX, mapY, mapX + getPreScale(), mapY + getPreScale()));
                        break;
                    }
                    default: {
                        log.warning("Found unrecognised buffer value at [" + x + "][" + y + "]: " + mapInfo.getBuffer()[x][y]);
                        break;
                    }
                }
            }
        }

        final List<List<Rect>> list = new ArrayList<>();
        list.add(floor);
        list.add(wall);
        list.add(carpet);

        return list;
    }

    public String getScaledHTMLCanvas(float width, float height) {
        return getScaledHTMLCanvas(width, height, false);
    }

    public String getScaledHTMLCanvas(float width, float height, boolean drawSpotAreas) {
        StringBuilder builder = new StringBuilder();
        updateScale(width, height);
        List<List<Rect>> rects = update();

        builder.append("mainContext.fillStyle='#AAAAAA';\n");
        for (Rect rect : rects.get(0)) {
            builder.append("mainContext.fillRect(" + rect.getLeft() + "," + rect.getTop() + "," + (rect.getLeft() - rect.getRight()) + "," + (rect.getTop() - rect.getBottom()) + ");\n");
        }

        builder.append("mainContext.fillStyle = '#000000';\n");
        for (Rect rect : rects.get(1)) {
            builder.append("mainContext.fillRect(" + rect.getLeft() + "," + rect.getTop() + "," + (rect.getLeft() - rect.getRight()) + "," + (rect.getTop() - rect.getBottom()) + ");\n");
        }

        builder.append("mainContext.fillStyle = '#DDDDDD';\n");
        for (Rect rect : rects.get(2)) {
            builder.append("mainContext.fillRect(" + rect.getLeft() + "," + rect.getTop() + "," + (rect.getLeft() - rect.getRight()) + "," + (rect.getTop() - rect.getBottom()) + ");\n");
        }

        if (drawSpotAreas && null != mapDetails.get(MapSetType.SpotArea)) {
            for (int i = 0; i < mapDetails.get(MapSetType.SpotArea).size(); i++) {
                MapDetail mapDetail = mapDetails.get(MapSetType.SpotArea).get(i);

                builder.append("mainContext.fillStyle = '#" + Integer.toHexString(new Random().nextInt(255)) + "" + Integer.toHexString(new Random().nextInt(255)) + "" + Integer.toHexString(new Random().nextInt(255)) + "';\n");
                builder.append("mainContext.beginPath();\n");
                builder.append("mainContext.moveTo(" + getPhoneX(mapDetail.getPositions().get(0).getOriginalX()) + ", " + getPhoneY(mapDetail.getPositions().get(0).getOriginalY()) + ")\n");
                for (Position position : mapDetail.getPositions()) {
                    builder.append("mainContext.lineTo(" + getPhoneX(position.getOriginalX()) + ", " + getPhoneY(position.getOriginalY()) + ");\n");
                }
                builder.append("mainContext.closePath();\n");
                builder.append("mainContext.fill();\n");

                builder.append("mainContext.fillStyle ='#FF00FF';\n");
                builder.append("mainContext.font = '30px Arial';\n");
                String name = mapDetail.getName() != null ? mapDetail.getName() : String.valueOf(mapDetail.getMid());
                builder.append("mainContext.fillText('" + name + "', " + getPhoneX(mapDetail.getPositions().get(0).getOriginalX()) + "," + getPhoneY(mapDetail.getPositions().get(0).getOriginalY()) + ")\n");
            }
        }

        return builder.toString();
    }

    public void writeHTML(String htmlFile, float width, float height, boolean paintRobot, boolean paintStation) throws Exception {
        updateScale(width, height);
        List<List<Rect>> rects = update();
        System.out.println(this);

        FileWriter writer = new FileWriter(htmlFile);
        writer.write("<!DOCTYPE html><html><head><title>Simple Canvas Example</title><style>canvas {border: 3px #CCC solid;}  </style></head><body><div id=\"container\"><canvas id=\"myCanvas\" height='" + height + "' width='" + width + "'></canvas></div><script>var mainCanvas = document.querySelector(\"#myCanvas\");var mainContext = mainCanvas.getContext(\"2d\");var canvasWidth = mainCanvas.width;var canvasHeight = mainCanvas.height;function drawCircle() {mainContext.clearRect(0, 0, canvasWidth, canvasHeight);\n");
        writer.write("mainContext.fillStyle='#AAAAAA';\n");

        for (Rect rect : rects.get(0)) {
            writer.write("mainContext.fillRect(" + rect.getLeft() + "," + rect.getTop() + "," + (rect.getLeft() - rect.getRight()) + "," + (rect.getTop() - rect.getBottom()) + ");\n");
        }
        writer.write("mainContext.fillStyle = '#000000';\n");
        for (Rect rect : rects.get(1)) {
            writer.write("mainContext.fillRect(" + rect.getLeft() + "," + rect.getTop() + "," + (rect.getLeft() - rect.getRight()) + "," + (rect.getTop() - rect.getBottom()) + ");\n");
        }
        writer.write("mainContext.fillStyle = '#DDDDDD';\n");
        for (Rect rect : rects.get(2)) {
            writer.write("mainContext.fillRect(" + rect.getLeft() + "," + rect.getTop() + "," + (rect.getLeft() - rect.getRight()) + "," + (rect.getTop() - rect.getBottom()) + ");\n");
        }

        String[] fillStyles = {"#FFFF00", "#00FFFF", "#FF00FF", "#ABCDEF", "#FEDCBA", "#EEEEEE"};
        if (null != mapDetails.get(MapSetType.SpotArea)) {
            for (MapDetail mapDetail : mapDetails.get(MapSetType.SpotArea)) {
                System.out.println("parsing " + mapDetail);
                writer.write("mainContext.fillStyle = '" + fillStyles[mapDetail.getMid()] + "';\n");
                writer.write("mainContext.beginPath();\n");
                writer.write("mainContext.moveTo(" + getPhoneX(mapDetail.getPositions().get(0).getOriginalX()) + ", " + getPhoneY(mapDetail.getPositions().get(0).getOriginalY()) + ")\n");
                for (Position position : mapDetail.getPositions()) {
                    writer.write("mainContext.lineTo(" + getPhoneX(position.getOriginalX()) + ", " + getPhoneY(position.getOriginalY()) + ");\n");
                }
                writer.write("mainContext.closePath();\n");
                writer.write("mainContext.fill();\n");
            }
        }

        if (paintRobot) {
            writer.write("mainContext.beginPath();\n");
            writer.write("mainContext.fillStyle = '#FF00FF';\n");
            writer.write("mainContext.arc(" + getPhoneX(botPosition.getOriginalX()) + ", " + getPhoneY(botPosition.getOriginalY()) + ", " + (2 * preScale) + ", 0, 2*Math.PI);\n");
            writer.write("mainContext.closePath();\n");
            writer.write("mainContext.fill();\n");
        }

        if (paintStation) {
            writer.write("mainContext.beginPath();\n");
            writer.write("mainContext.fillStyle = '#00FF00';\n");
            writer.write("mainContext.fillRect(" + (getPhoneX(chargerPosition.getOriginalX()) - 10) + ", " + getPhoneY(chargerPosition.getOriginalY()) + ", " + (2 * preScale) + ", " + (2 * preScale) + ");\n");
            writer.write("mainContext.closePath();\n");
        }

        writer.write("mainContext.closePath();}drawCircle();</script></body></html>");
        writer.close();
    }

    @Data
    @AllArgsConstructor
    public class Rect {
        private final float left;
        private final float top;
        private final float right;
        private final float bottom;
    }
}
