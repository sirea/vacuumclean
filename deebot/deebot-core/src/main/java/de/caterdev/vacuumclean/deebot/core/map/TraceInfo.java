/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vacuumclean.deebot.core.map;

import de.caterdev.vacuumclean.deebot.core.DataParseUtils;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class TraceInfo {
    public static final String ERROR_WRONG_TRACE_ID = "Trying to update trace with id %s with given traceId %s\n";

    private int count = -1;
    private List<TracePoint> points = new ArrayList<>();
    private int traceId = -1;
    private boolean initialised = false;

    public void updateTrace(int traceId, int traceFrom, int traceTo, String traceData) throws Exception {
        updateTrace(traceId, traceFrom, traceTo, DataParseUtils.parseTracePoints(traceData));
    }

    public void updateTrace(int traceId, int tf, int tt, List<TracePoint> tracePoints) {
        if (this.traceId != traceId) {
            if (initialised) {
                throw new RuntimeException(String.format(ERROR_WRONG_TRACE_ID, this.traceId, traceId));
            } else {
                initialised = true;
                this.traceId = traceId;
                this.count = 0;
                this.points = new ArrayList<>();
            }
        }

        // if the traceFrom is larger than the currently existing trace, we have to fill it up with null-values
        if (tf > this.points.size()) {
            for (int i = this.points.size(); i < tf; i++) {
                this.points.add(null);
            }
        }

        for (int i = tf; i <= tt; i++) {
            TracePoint tracePoint = tracePoints.get(i - tf);
            this.points.add(i, tracePoint);
        }

        /* TODO:
            if (this.points.size() > i) {
                this.points.remove(i);
            } else {
                this.points.add(i, tracePoint);
            }
         */
    }


}
