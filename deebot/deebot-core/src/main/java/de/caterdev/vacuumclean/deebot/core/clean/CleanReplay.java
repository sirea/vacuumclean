/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vacuumclean.deebot.core.clean;

import de.caterdev.vacuumclean.deebot.core.map.MapInfo;
import de.caterdev.vacuumclean.deebot.core.map.TraceInfo;
import de.caterdev.vacuumclean.deebot.core.map.TracePoint;
import de.caterdev.vacuumclean.deebot.core.ui.CleanMapData;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.io.FileWriter;

@AllArgsConstructor
@Data
public class CleanReplay {
    private final CleanMapData cleanMapData;
    private final MapInfo mapInfo;
    private final TraceInfo traceInfo;

    public void writeUnscaledHTML(String filename) throws Exception {
        FileWriter writer = new FileWriter(filename);
        writer.write("<!DOCTYPE html><html><head><title>Simple Canvas Example</title><style>canvas {border: 3px #CCC solid;}  </style></head><body><div id=\"container\"><canvas id=\"myCanvas\" height='" + (mapInfo.getHeight()) + "' width='" + (mapInfo.getWidth()) + "'></canvas></div>");
        writer.write("<script>var mainCanvas = document.querySelector(\"#myCanvas\");var mainContext = mainCanvas.getContext(\"2d\");var canvasWidth = mainCanvas.width;var canvasHeight = mainCanvas.height;");
        writer.write("var requestAnimationFrame = window.requestAnimationFrame || window.mozRequestAnimationFrame || window.webkitRequestAnimationFrame || window.msRequestAnimationFrame;\n");

        writer.write("var counter=0;\n");
        writer.write("var clockTicker=0;\n");
        writer.write("var positions=[");
        for (TracePoint tracePoint : traceInfo.getPoints()) {
            if (null != tracePoint) {
                // TODO: die +200 noch rausnehmen
                //writer.write("[" + (tracePoint.getPositionX() + 400) + ", " + (tracePoint.getPositionY() + 200) + "],");
                writer.write("[" + (cleanMapData.getPhoneX(tracePoint.getPositionX())) + ", " + (cleanMapData.getPhoneY(tracePoint.getPositionY())) + "],");
            }
        }
        writer.write("]\n");

        writer.write("function drawCircle() {mainContext.clearRect(0, 0, canvasWidth, canvasHeight);\n");

        writer.write("mainContext.beginPath();var radius = 10;\n");
        writer.write("var positionX = positions[counter][0];\n");
        writer.write("var positionY = positions[counter][1];\n");
        writer.write("mainContext.arc(positionX, positionY, radius, 0, 2*Math.PI, false);\n");
        // color in the circle
        writer.write("mainContext.fillStyle = '#006699';\n");
        writer.write("mainContext.fill();\n");


        writer.write("mainContext.lineJoin = 'round';");
        writer.write("mainContext.moveTo(positions[0][0], positions[0][1]);\n");
        writer.write("for (i = 1; i < counter; i++) {\n");
        writer.write("mainContext.lineTo(positions[i][0], positions[i][1]);\n");
        writer.write("mainContext.moveTo(positions[i][0], positions[i][1]);\n");
        writer.write("mainContext.stroke();\n");
        writer.write("}\n");


        byte type = -1;
        for (int i = 0; i < mapInfo.getBuffer().length; i++) {
            for (int j = 0; j < mapInfo.getBuffer()[i].length; j++) {
                byte mapBuffer = mapInfo.getBuffer()[i][j];

                if (mapBuffer == MapInfo.NODATA && type != MapInfo.NODATA) {
                    continue;
                }

                if (mapBuffer == MapInfo.WALL && type != MapInfo.WALL) {
                    writer.write("mainContext.fillStyle = '#000000';\n");
                    type = MapInfo.WALL;
                } else if (mapBuffer == MapInfo.FLOOR && type != MapInfo.FLOOR) {
                    writer.write("mainContext.fillStyle = '#FFFFAA';\n");
                    type = MapInfo.FLOOR;
                } else if (mapBuffer == MapInfo.CARPET && type != MapInfo.CARPET) {
                    writer.write("mainContext.fillStyle = '#ABABAB';\n");
                    type = MapInfo.CARPET;
                }

                writer.write("mainContext.fillRect(" + i + "," + j + "," + 1 + "," + 1 + ");\n");

            }
        }


        writer.write("mainContext.closePath();");
        writer.write("if (clockTicker % 1 == 0) {\n");
        writer.write("counter++;\n");
        writer.write("if (counter > positions.length - 1) {\n");
        writer.write("counter = 1;\n");
        writer.write("}\n");
        writer.write("}\n");
        writer.write("clockTicker++;\n");

        writer.write("requestAnimationFrame(drawCircle)};\n");
        writer.write("drawCircle();</script></body></html>");
        writer.close();
    }
}
