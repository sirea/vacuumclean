/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vaccumclean.deebot.smack.packets.info;

import de.caterdev.vacuumclean.deebot.core.UserInfo;
import org.jivesoftware.smack.util.XmlStringBuilder;
import org.jxmpp.jid.Jid;

import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

/**
 * <iq to='$user' type='set' id='$id'>
 * <query xmlns='com:ctl'>
 * <ctl td='UserInfos'>
 * <u j='jid1' a='userman,setting,clean'/>
 * <u j='$jid2' a=''/>
 * <u j='$jid3' a='userman,setting,clean'/>
 * </ctl>
 * </query>
 * </iq>
 */
public class UserInfosPacket extends InfoPacket {
    public static final String INFO = "UserInfos";

    public static final String ELEMENT_USER = "u";
    public static final String ATTRIBUTE_JID = "j";
    public static final String ATTRIBUTE_RIGHTS = "a";

    private final Collection<UserInfo> userInfos;

    public UserInfosPacket(Jid to, UserInfo... userInfos) {
        super(to, false, true);

        this.userInfos = Arrays.asList(userInfos);
    }

    public UserInfosPacket(String innerId, UserInfo... userInfos) {
        super(innerId, false, true);

        this.userInfos = Arrays.asList(userInfos);
    }

    @Override
    protected XmlStringBuilder getAdditionalElements(XmlStringBuilder xml) {


        for (UserInfo userInfo : userInfos) {
            String rights = userInfo.getUserRights().stream().map(Enum::toString).collect(Collectors.joining(","));

            xml.halfOpenElement(ELEMENT_USER);
            xml.attribute(ATTRIBUTE_JID, userInfo.getJid().asUnescapedString());
            xml.attribute(ATTRIBUTE_RIGHTS, rights);
            xml.closeEmptyElement();
        }

        return xml;
    }

    public Collection<UserInfo> getUserInfos() {
        return userInfos;
    }

    @Override
    protected String getInfo() {
        return INFO;
    }
}