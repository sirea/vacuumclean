/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vaccumclean.deebot.smack.packets.action;

import org.jivesoftware.smack.util.XmlStringBuilder;
import org.jxmpp.jid.Jid;

import java.util.HashMap;
import java.util.Map;

/**
 * <iq type='set' id='$if' from='$admin' to='$bot'><query xmlns='com:ctl'><ctl td='SetAC' id='$innerId' jid='$newUser'><acs><ac name="userman" allow="1"/><ac name="setting" allow="1"/><ac name="clean" allow="1"/></acs></ctl></query></iq>
 */
public class SetACPacket extends ActionPacket {
    public static final String ACTION = "SetAC";

    public static final String ATTRIBUTE_NEW_CLIENT = "jid";
    public static final String ELEMENT_ACS = "acs";
    public static final String ELEMENT_AC = "ac";
    public static final String ATTRIBUTE_NAME = "name";
    public static final String VALUE_USERMAN = "userman";
    public static final String VALUE_SETTING = "setting";
    public static final String VALUE_CLEAN = "clean";
    public static final String ATTRIBUTE_ALLOW = "allow";
    private final Jid newClient;
    private final boolean userman;
    private final boolean setting;
    private final boolean clean;

    public SetACPacket(Jid from, Jid to, Jid newClient, boolean userman, boolean setting, boolean clean) {
        super(from, to, true, true);

        this.newClient = newClient;
        this.userman = userman;
        this.setting = setting;
        this.clean = clean;
    }
    public SetACPacket(String innerId, Jid newClient, boolean userman, boolean setting, boolean clean) {
        super(innerId, true, true);

        this.newClient = newClient;
        this.userman = userman;
        this.setting = setting;
        this.clean = clean;
    }

    public Jid getNewClient() {
        return newClient;
    }

    public boolean isUserman() {
        return userman;
    }

    public boolean isSetting() {
        return setting;
    }

    public boolean isClean() {
        return clean;
    }

    @Override
    protected Map<String, String> getAdditionalAttributes() {
        Map<String, String> additionalAttributes = new HashMap<>(1);
        additionalAttributes.put(ATTRIBUTE_NEW_CLIENT, newClient.asUnescapedString());

        return additionalAttributes;
    }

    @Override
    protected XmlStringBuilder getAdditionalElements(XmlStringBuilder xml) {
        xml.openElement(ELEMENT_ACS);

        xml.halfOpenElement(ELEMENT_AC);
        xml.attribute(ATTRIBUTE_NAME, VALUE_USERMAN);
        xml.attribute(ATTRIBUTE_ALLOW, userman ? "1" : "0");
        xml.closeEmptyElement();

        xml.halfOpenElement(ELEMENT_AC);
        xml.attribute(ATTRIBUTE_NAME, VALUE_SETTING);
        xml.attribute(ATTRIBUTE_ALLOW, setting ? "1" : "0");
        xml.closeEmptyElement();

        xml.halfOpenElement(ELEMENT_AC);
        xml.attribute(ATTRIBUTE_NAME, VALUE_CLEAN);
        xml.attribute(ATTRIBUTE_ALLOW, clean ? "1" : "0");
        xml.closeEmptyElement();

        xml.closeElement(ELEMENT_ACS);

        return xml;
    }

    @Override
    protected String getAction() {
        return ACTION;
    }
}
