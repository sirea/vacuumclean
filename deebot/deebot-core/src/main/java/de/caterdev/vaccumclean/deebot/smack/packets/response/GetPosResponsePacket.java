/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vaccumclean.deebot.smack.packets.response;

import de.caterdev.vacuumclean.deebot.core.map.Position;
import org.xmlpull.v1.XmlPullParser;

import java.util.HashMap;
import java.util.Map;

/**
 * <iq to='$to' type='set' id='$id'><query xmlns='com:ctl'><ctl id='$innerId' ret='$retValue' p='$position' a='$angle'/></query></iq> -- general position
 * <iq to='$to' type='set' id='$id'><query xmlns='com:ctl'><ctl id='$innerId' ret='$retValue' t='$t' p='$position' a='$angle' valid='$valid'/></query></iq> -- device position
 */
public class GetPosResponsePacket extends ResponsePacket {
    public static final String ATTRIBUTE_POSITION = "p";
    public static final String ATTRIBUTE_ANGLE = "a";
    public static final String ATTRIBUTE_TYPE = "t";
    public static final String ATTRIBUTE_VALID = "valid";

    private final String position;
    private final String angle;
    private final String type;
    private final String valid;
    private final Position pos;


    public GetPosResponsePacket(String innerId, RetValue retValue, XmlPullParser parser) {
        super(innerId, retValue, true, false);

        this.position = parser.getAttributeValue(null, ATTRIBUTE_POSITION);
        this.angle = parser.getAttributeValue(null, ATTRIBUTE_ANGLE);
        this.type = parser.getAttributeValue(null, ATTRIBUTE_TYPE);
        this.valid = parser.getAttributeValue(null, ATTRIBUTE_VALID);

        this.pos = new Position(position, angle);
    }

    public static boolean isPacket(XmlPullParser parser) throws Exception {
        boolean result = parser.getName().equals(ELEMENT_CTL);
        result &= parser.getEventType() == XmlPullParser.START_TAG;
        result &= null != parser.getAttributeValue(null, ATTRIBUTE_POSITION);
        result &= null != parser.getAttributeValue(null, ATTRIBUTE_ANGLE);

        return result;
    }

    @Override
    protected Map<String, String> getAdditionalAttributes() {
        Map<String, String> additionalAttributes = new HashMap<>(2);
        additionalAttributes.put(ATTRIBUTE_POSITION, position);
        additionalAttributes.put(ATTRIBUTE_ANGLE, angle);

        if (null != type) {
            additionalAttributes.put(ATTRIBUTE_TYPE, type);
            additionalAttributes.put(ATTRIBUTE_VALID, valid);
        }

        return additionalAttributes;
    }

    public Position getPos() {
        return pos;
    }
}
