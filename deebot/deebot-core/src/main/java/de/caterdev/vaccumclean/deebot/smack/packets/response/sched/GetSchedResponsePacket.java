/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vaccumclean.deebot.smack.packets.response.sched;

import de.caterdev.vaccumclean.deebot.smack.packets.action.sched.AbstractSchedPacket;
import de.caterdev.vaccumclean.deebot.smack.packets.response.ResponsePacket;
import de.caterdev.vaccumclean.deebot.smack.packets.response.SimpleResponsePacket;
import de.caterdev.vacuumclean.deebot.core.clean.CleanSchedule;
import lombok.Getter;
import org.jivesoftware.smack.util.XmlStringBuilder;
import org.xmlpull.v1.XmlPullParser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <iq to='$to' type='set' id='$id'><query xmlns='com:ctl'><ctl id='$innerId' ret='ok'>
 * <s n='timing_name_360006138100' o='0' h='10' m='0' r='0000000' f='p'><ctl td='Clean'><clean type='auto'/></ctl></s>
 * <s n='timing_name_36000231100' o='1' h='10' m='0' r='0000000' f='q'><ctl td='Clean'><clean type='auto'/></ctl></s>
 * <s n='timing_name_360006179100' o='1' h='10' m='0' r='0000000' f='q'><ctl td='Clean'><clean type='auto'/></ctl></s>
 * </ctl></query></iq>
 */
public class GetSchedResponsePacket extends ResponsePacket {
    @Getter
    private List<CleanSchedule> cleanSchedules = new ArrayList<>();
    @Getter
    private Map<String, CleanSchedule> cleanSchedulesMap;

    public GetSchedResponsePacket(String innerId, RetValue retValue, XmlPullParser parser) {
        super(innerId, retValue, false, true);

        try {
            cleanSchedules = new ArrayList<>();
            cleanSchedulesMap = new HashMap<>();
            while (parser.getName().equals(AbstractSchedPacket.ELEMENT_SETTINGS) && parser.getEventType() == XmlPullParser.START_TAG) {
                CleanSchedule cleanSchedule = AbstractSchedPacket.parseContents(parser);
                cleanSchedules.add(cleanSchedule);
                cleanSchedulesMap.put(cleanSchedule.getName(), cleanSchedule);

                parser.nextTag();
                parser.nextTag();
                parser.nextTag();
                parser.nextTag();
            }
        } catch (final Exception e) {
            throw new RuntimeException("Could not parse " + parser);
        }
    }

    public GetSchedResponsePacket(SimpleResponsePacket packet) {
        super(packet.getInnerId(), packet.getRetValue(), false, true);

        this.cleanSchedulesMap = new HashMap<>();
        this.cleanSchedules = new ArrayList<>();
    }

    @Override
    protected XmlStringBuilder getAdditionalElements(XmlStringBuilder xml) {
        for (CleanSchedule cleanSchedule : cleanSchedules) {
            xml = AbstractSchedPacket.buildCleanSchedule(xml, cleanSchedule);
        }
        return xml;
    }
}
