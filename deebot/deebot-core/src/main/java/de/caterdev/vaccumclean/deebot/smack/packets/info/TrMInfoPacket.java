/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vaccumclean.deebot.smack.packets.info;

import java.util.HashMap;
import java.util.Map;

/**
 * <iq to='$to' type='set' id='$id'><query xmlns='com:ctl'><ctl td='TrM' trid='$traceId' c='$count'/></query></iq>
 */
public class TrMInfoPacket extends InfoPacket {
    public static final String INFO = "TrM";

    public static final String ATTRIBUTE_TRACE_ID = "trid";
    public static final String ATTRIBUTE_COUNT = "c";

    private final String traceId;
    private final String count;

    public TrMInfoPacket(String innerId, String traceId, String count) {
        super(innerId, true, false);

        this.traceId = traceId;
        this.count = count;
    }

    @Override
    protected Map<String, String> getAdditionalAttributes() {
        Map<String, String> addtionalAttributes = new HashMap<>();

        addtionalAttributes.put(ATTRIBUTE_TRACE_ID, traceId);
        addtionalAttributes.put(ATTRIBUTE_COUNT, count);

        return addtionalAttributes;
    }

    @Override
    protected String getInfo() {
        return INFO;
    }
}
