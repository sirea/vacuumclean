/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vaccumclean.deebot.smack.packets.response;

import de.caterdev.vacuumclean.deebot.core.BlockTime;
import org.jivesoftware.smack.util.XmlStringBuilder;
import org.jxmpp.jid.Jid;

/**
 * <iq xmlns='jabber:client' to='$user' id='$id' type='set'><query xmlns='com:ctl'><ctl id='$innerId' ret='$ret'><bt sh='$startHour' sm='$startMinute' eh='$endHour' em='$endMinute'/></ctl></query></iq>
 */
public class GetBlockTimeResponsePacket extends ResponsePacket {
    public static final String ELEMENT_BLOCKTIME = "bt";
    public static final String ATTRIBUTE_START_HOUR = "sh";
    public static final String ATTRIBUTE_START_MINUTE = "sm";
    public static final String ATTRIBUTE_END_HOUR = "eh";
    public static final String ATTRIBUTE_END_MINUTE = "em";

    private final String startHour;
    private final String startMinute;
    private final String endHour;
    private final String endMinute;

    private final BlockTime blockTime;

    public GetBlockTimeResponsePacket(String innerId, RetValue retValue, String startHour, String startMinute, String endHour, String endMinute) {
        super(innerId, retValue, false, true);

        this.startHour = startHour;
        this.startMinute = startMinute;
        this.endHour = endHour;
        this.endMinute = endMinute;

        this.blockTime = new BlockTime(startHour, startMinute, endHour, endMinute);
    }

    public GetBlockTimeResponsePacket(Jid to, RetValue retValue, String startHour, String startMinute, String endHour, String endMinute) {
        super(to, retValue, false, true);

        this.startHour = startHour;
        this.startMinute = startMinute;
        this.endHour = endHour;
        this.endMinute = endMinute;

        this.blockTime = new BlockTime(startHour, startMinute, endHour, endMinute);
    }

    public BlockTime getBlockTime() {
        return blockTime;
    }

    @Override
    protected XmlStringBuilder getAdditionalElements(XmlStringBuilder xml) {
        // TODO: prüfen, ob auch mehrer Block Times kommen können!

        xml.halfOpenElement(ELEMENT_BLOCKTIME);
        xml.attribute(ATTRIBUTE_START_HOUR, startHour);
        xml.attribute(ATTRIBUTE_START_MINUTE, startMinute);
        xml.attribute(ATTRIBUTE_END_HOUR, endHour);
        xml.attribute(ATTRIBUTE_END_MINUTE, endMinute);
        xml.closeEmptyElement();

        return xml;
    }
}
