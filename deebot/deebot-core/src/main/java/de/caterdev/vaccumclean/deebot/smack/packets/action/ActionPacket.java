/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vaccumclean.deebot.smack.packets.action;

import de.caterdev.vaccumclean.deebot.smack.packets.Query;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.id.StanzaIdUtil;
import org.jivesoftware.smack.util.XmlStringBuilder;
import org.jxmpp.jid.Jid;

/**
 * Action packets are sent to the bot and usually generate a reply, e.g. containing information about bot status or results of activities.
 *
 * @param <T> the type of the response packet
 */
public abstract class ActionPacket<T extends Query> extends Query {
    public static final IQ.Type type = IQ.Type.set;

    private ActionResponseListener<T> actionResponseListener;

    public ActionPacket(String ctlStanzaId, boolean additionalAttributes, boolean additionalElements) {
        super(ctlStanzaId, false, true, additionalAttributes, additionalElements);
    }

    public ActionPacket(Jid from, Jid to, boolean additionalAttributes, boolean additionalElements) {
        super(StanzaIdUtil.newStanzaId(), false, true, additionalAttributes, additionalElements);

        setFrom(from);
        setTo(to);
        setType(type);
    }

    public ActionResponseListener<T> getActionResponseListener() {
        return actionResponseListener;
    }

    public void setActionResponseListener(ActionResponseListener<T> actionResponseListener) {
        this.actionResponseListener = actionResponseListener;
    }

    @Override
    protected XmlStringBuilder getCtlAttributes(XmlStringBuilder xml) {
        xml.attribute(ATTRIBUTE_ID, innerId);
        xml.attribute(ATTRIBUTE_TD, getAction());

        return xml;
    }

    protected abstract String getAction();
}
