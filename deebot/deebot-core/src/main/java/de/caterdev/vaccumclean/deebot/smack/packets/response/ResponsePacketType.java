/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vaccumclean.deebot.smack.packets.response;

import de.caterdev.vaccumclean.deebot.smack.packets.response.clean.GetCleanSumResponsePacket;
import de.caterdev.vaccumclean.deebot.smack.packets.response.map.GetMapBuildStateResponsePacket;
import de.caterdev.vaccumclean.deebot.smack.packets.response.map.GetMapSetResponsePacket;
import de.caterdev.vaccumclean.deebot.smack.packets.response.map.PullMResponsePacket;
import org.xmlpull.v1.XmlPullParser;

public enum ResponsePacketType {
    traceInfoResponsePacket, traceModelResponsePacket, mapBuildStateResponsePacket, mapSetResponsePacket, cleanSumResponsePacket,
    pullMResponsePacket, activeLanguageResponsePacket, unknown;

    public static ResponsePacketType getType(XmlPullParser parser) {
        assert parser != null;

        if (parser.getAttributeCount() == 3 && parser.getAttributeValue(null, GetTraceInfoResponsePacket.ATTRIBUTE_TRACE_DATA) != null) {
            return traceInfoResponsePacket;
        } else if (parser.getAttributeCount() == 4 && parser.getAttributeValue(null, GetMapBuildStateResponsePacket.ATTRIBUTE_MAP_METHOD) != null && parser.getAttributeValue(null, GetMapBuildStateResponsePacket.ATTRIBUTE_MAP_STATUS) != null) {
            return mapBuildStateResponsePacket;
        } else if (parser.getAttributeCount() == 4 && null != parser.getAttributeValue(null, GetTraceModelResponsePacket.ATTRIBUTE_TRACE_ID) && null != parser.getAttributeValue(null, GetTraceModelResponsePacket.ATTRIBUTE_COUNT)) {
            return traceModelResponsePacket;
        } else if (parser.getAttributeCount() == 4 && null != parser.getAttributeValue(null, GetMapSetResponsePacket.ATTRIBUTE_MAP_SET_ID) && null != parser.getAttributeValue(null, GetMapSetResponsePacket.ATTRIBUTE_MAP_SET_TYPE)) {
            return mapSetResponsePacket;
        } else if (parser.getAttributeCount() == 5 && null != parser.getAttributeValue(null, GetCleanSumResponsePacket.ATTRIBUTE_COUNT) && null != parser.getAttributeValue(null, GetCleanSumResponsePacket.ATTRIBUTE_DURATION) && null != parser.getAttributeValue(null, GetCleanSumResponsePacket.ATTRIBUTE_CLEANED_AREA)) {
            return cleanSumResponsePacket;
        } else if (parser.getAttributeCount() == 2 && null != parser.getAttributeValue(null, GetActiveLanguageResponsePacket.ATTRIBUTE_LANGUAGE)) {
            return activeLanguageResponsePacket;
        } else if (parser.getAttributeCount() == 3 && null != parser.getAttributeValue(null, PullMResponsePacket.ATTRIBUTE_POSITIONS)) {
            return pullMResponsePacket;
        }

        return unknown;
    }
}
