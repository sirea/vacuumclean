/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2020, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vaccumclean.deebot.smack.packets.action.admin;

import de.caterdev.vaccumclean.deebot.smack.packets.action.ActionPacket;
import de.caterdev.vaccumclean.deebot.smack.packets.response.SimpleResponsePacket;
import org.jivesoftware.smack.util.XmlStringBuilder;
import org.jxmpp.jid.Jid;
import org.xmlpull.v1.XmlPullParser;

/**
 * <iq to='$bot' from='$user' id='$id' type='set'><query xmlns='com:ctl'><ctl id='$innerId' td='SetTime'><time t='$time' tz='$timeZone'/></query></iq>
 */
public class SetTimePacket extends ActionPacket<SimpleResponsePacket> {
    public static final String ACTION = "SetTime";

    public static final String ELEMENT_TIME = "time";
    public static final String ATTRIBUTE_TIME = "t";
    public static final String ATTRIBUTE_TIMEZONE = "tz";
    private final long time;
    private final int timeZone;

    public SetTimePacket(String innerId, XmlPullParser parser) throws Exception {
        super(innerId, false, true);

        parser.next();
        this.time = Long.parseLong(parser.getAttributeValue(null, ATTRIBUTE_TIME));
        this.timeZone = Integer.parseInt(parser.getAttributeValue(null, ATTRIBUTE_TIMEZONE));
    }

    public SetTimePacket(Jid from, Jid to, long time, int timeZone) {
        super(from, to, false, true);

        this.time = time;
        this.timeZone = timeZone;
    }

    @Override
    protected XmlStringBuilder getAdditionalElements(XmlStringBuilder xml) {
        xml.halfOpenElement(ELEMENT_TIME);
        xml.attribute(ATTRIBUTE_TIME, String.valueOf(time));
        xml.attribute(ATTRIBUTE_TIMEZONE, String.valueOf(timeZone));
        xml.closeEmptyElement();

        return xml;
    }

    @Override
    protected String getAction() {
        return ACTION;
    }
}
