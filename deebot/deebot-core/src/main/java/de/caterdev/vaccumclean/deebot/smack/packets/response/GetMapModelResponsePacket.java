/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vaccumclean.deebot.smack.packets.response;

import de.caterdev.vacuumclean.deebot.core.map.MapModel;
import lombok.Getter;
import org.xmlpull.v1.XmlPullParser;

import java.util.HashMap;
import java.util.Map;

/**
 * <iq to='$user' type='set' id='$id'><query xmlns='com:ctl'><ctl id='$innerId' i='$mapId' w='$columnGrid' h='$rowGrid' r='$rowPiece' c='$columnPiece' p='$pixelWidth' m='$crc[]'/></query></iq>
 *
 * A map model describes the underlying map of the robot. For additional details about the values see {@link MapModel}
 */
public class GetMapModelResponsePacket extends ResponsePacket {
    public static final String ATTRIBUTE_MAP_ID = "i";
    public static final String ATTRIBUTE_COLUMN_GRID = "w";
    public static final String ATTRIBUTE_ROW_GRID = "h";
    public static final String ATTRIBUTE_ROW_PIECE = "r";
    public static final String ATTRIBUTE_COLUMN_PIECE = "c";
    public static final String ATTRIBUTE_PIXEL_WIDTH = "p";
    public static final String ATTRIBUTE_CRC = "m";


    private final String mapId;
    private final String columnGrid;
    private final String rowGrid;
    private final String columnPiece;
    private final String rowPiece;
    private final String pixelWidth;
    private final String crc;

    @Getter
    private final MapModel mapModel;

    public GetMapModelResponsePacket(String innerId, ResponsePacket.RetValue retValue, String mapId, String columnGrid, String rowGrid, String columnPiece, String rowPiece, String pixelWidth, String crc) {
        super(innerId, retValue, true, false);

        this.mapId = mapId;
        this.pixelWidth = pixelWidth;
        this.columnGrid = columnGrid;
        this.columnPiece = columnPiece;
        this.rowGrid = rowGrid;
        this.rowPiece = rowPiece;
        this.crc = crc;

        /**
         * TODO: parameter o seems to exist, too
         * String nodeAttribute2 = DomUtils.getInstance().getNodeAttribute(element, "o");
         *                             if (!TextUtils.isEmpty(nodeAttribute2)) {
         *                                 String[] split2 = nodeAttribute2.split(",");
         *                                 if (split2.length >= 4) {
         *                                     mapM.boxTopLeft = new Position(Float.valueOf(split2[0]).floatValue(), Float.valueOf(split2[1]).floatValue());
         *                                     mapM.boxButtomRight = new Position(Float.valueOf(split2[2]).floatValue(), Float.valueOf(split2[3]).floatValue());
         *                                 }
         *                             }
         */

        this.mapModel = new MapModel(mapId, columnGrid, rowGrid, columnPiece, rowPiece, pixelWidth, crc);
    }

    public GetMapModelResponsePacket(String innerId, XmlPullParser parser) {
        super(innerId, parser, true, false);

        this.mapId = parser.getAttributeValue(null, ATTRIBUTE_MAP_ID);
        this.columnGrid = parser.getAttributeValue(null, ATTRIBUTE_COLUMN_GRID);
        this.rowGrid = parser.getAttributeValue(null, ATTRIBUTE_ROW_GRID);
        this.columnPiece = parser.getAttributeValue(null, ATTRIBUTE_COLUMN_PIECE);
        this.rowPiece = parser.getAttributeValue(null, ATTRIBUTE_ROW_PIECE);
        this.pixelWidth = parser.getAttributeValue(null, ATTRIBUTE_PIXEL_WIDTH);
        this.crc = parser.getAttributeValue(null, ATTRIBUTE_CRC);

        this.mapModel = new MapModel(mapId, columnGrid, rowGrid, columnPiece, rowPiece, pixelWidth, crc);
    }

    public static boolean isPacket(XmlPullParser parser) throws Exception {
        if (parser.getName().equals(ELEMENT_CTL) && parser.getEventType() == XmlPullParser.START_TAG) {
            return parser.getAttributeValue(null, ATTRIBUTE_MAP_ID) != null && parser.getAttributeValue(null, ATTRIBUTE_COLUMN_GRID) != null;
        }

        return false;
    }

    @Override
    protected Map<String, String> getAdditionalAttributes() {
        Map<String, String> additionalAttributes = new HashMap<>();
        additionalAttributes.put(ATTRIBUTE_MAP_ID, mapId);
        additionalAttributes.put(ATTRIBUTE_COLUMN_GRID, columnGrid);
        additionalAttributes.put(ATTRIBUTE_ROW_GRID, rowGrid);
        additionalAttributes.put(ATTRIBUTE_COLUMN_PIECE, columnPiece);
        additionalAttributes.put(ATTRIBUTE_ROW_PIECE, rowPiece);
        additionalAttributes.put(ATTRIBUTE_PIXEL_WIDTH, pixelWidth);
        additionalAttributes.put(ATTRIBUTE_CRC, crc);

        return additionalAttributes;
    }
}
