/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vaccumclean.deebot.smack.packets.info;

import de.caterdev.vaccumclean.deebot.smack.packets.response.GetChargeStateResponsePacket;
import org.jivesoftware.smack.util.XmlStringBuilder;

import java.util.HashMap;
import java.util.Map;

/**
 * <iq to='$to' type='set' id='$id'><query xmlns='com:ctl'><ctl ts='$innerId' td='ChargeState'><charge type='Idle' h='' r='' s=''/></ctl></query></iq>
 */
public class ChargeStateInfoPacket extends InfoPacket {
    public static final String INFO = "ChargeState";

    public static final String ELEMENT_CHARGE = "charge";
    public static final String ATTRIBUTE_TYPE = "type";
    public static final String ATTRIBUTE_H = "h";
    public static final String ATTRIBUTE_R = "r";
    public static final String ATTRIBUTE_S = "s";

    private final GetChargeStateResponsePacket.ChargeType chargeType;
    private final String chargeGoingReason;
    private final String isNeedHelp;
    private final String isContinueClean;

    private final String tsValue;

    public ChargeStateInfoPacket(String innerId, String tsValue, String chargeType, String chargeGoingReason, String isNeedHelp, String isContinueClean) {
        super(innerId, true, true);

        this.chargeType = GetChargeStateResponsePacket.ChargeType.valueOf(chargeType);
        this.chargeGoingReason = chargeGoingReason;
        this.isNeedHelp = isNeedHelp;
        this.isContinueClean = isContinueClean;

        this.tsValue = tsValue;
    }

    @Override
    protected XmlStringBuilder getAdditionalElements(XmlStringBuilder xml) {
        xml.halfOpenElement(ELEMENT_CHARGE);
        xml.attribute(ATTRIBUTE_TYPE, chargeType.name());
        xml.attribute(ATTRIBUTE_H, isNeedHelp);
        xml.attribute(ATTRIBUTE_R, chargeGoingReason);
        xml.attribute(ATTRIBUTE_S, isContinueClean);
        xml.closeEmptyElement();

        return xml;
    }

    @Override
    protected Map<String, String> getAdditionalAttributes() {
        Map<String, String> additionalAttributes = new HashMap<>();
        additionalAttributes.put("ts", tsValue);

        return additionalAttributes;
    }

    @Override
    protected String getInfo() {
        return INFO;
    }

    public enum ChargeGoingReason {
        APP("a");

        private final String reason;

        ChargeGoingReason(String reason) {
            this.reason = reason;
        }
    }

}
