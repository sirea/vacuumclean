/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vaccumclean.deebot.smack.packets.action;

import de.caterdev.vaccumclean.deebot.smack.packets.response.GetTraceInfoResponsePacket;
import org.jxmpp.jid.Jid;
import org.xmlpull.v1.XmlPullParser;

import java.util.HashMap;
import java.util.Map;

/**
 * <iq id="$id" to="$to" from="$from" type="set"><query xmlns="com:ctl"><ctl id="$innerId" td="GetTr" trid="$traceId" tf="$traceFrom" tt="$traceTo"/></query></iq>
 */
public class GetTraceInfoPacket extends ActionPacket<GetTraceInfoResponsePacket> {
    public static final String ACTION = "GetTr";

    public static final String ATTRIBUTE_TRACE_ID = "trId";
    public static final String ATTRIBUTE_TRACE_FROM = "tf";
    public static final String ATTRIBUTE_TRACE_TO = "tt";


    private final String traceId;
    private final String traceFrom;
    private final String traceTo;

    public GetTraceInfoPacket(String innerId, XmlPullParser parser) {
        super(innerId, true, false);

        this.traceId = parser.getAttributeValue(null, ATTRIBUTE_TRACE_ID);
        this.traceFrom = parser.getAttributeValue(null, ATTRIBUTE_TRACE_FROM);
        this.traceTo = parser.getAttributeValue(null, ATTRIBUTE_TRACE_TO);
    }

    public GetTraceInfoPacket(Jid from, Jid to, String traceId, String traceFrom, String traceTo) {
        super(from, to, true, false);
        this.traceId = traceId;
        this.traceFrom = traceFrom;
        this.traceTo = traceTo;
    }

    @Override
    protected Map<String, String> getAdditionalAttributes() {
        Map<String, String> additionalAttributes = new HashMap<>();
        additionalAttributes.put(ATTRIBUTE_TRACE_ID, traceId);
        additionalAttributes.put(ATTRIBUTE_TRACE_FROM, traceFrom);
        additionalAttributes.put(ATTRIBUTE_TRACE_TO, traceTo);

        return additionalAttributes;
    }

    @Override
    protected String getAction() {
        return ACTION;
    }
}
