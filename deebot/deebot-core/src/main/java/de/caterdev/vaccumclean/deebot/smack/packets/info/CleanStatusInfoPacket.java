/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vaccumclean.deebot.smack.packets.info;

import de.caterdev.vacuumclean.deebot.core.clean.CleanStatistics;
import de.caterdev.vacuumclean.deebot.core.constants.CleanStartReason;
import lombok.Getter;
import org.xmlpull.v1.XmlPullParser;

import java.util.HashMap;
import java.util.Map;

/**
 * <iq to='$to' type='set' id='$id'><query xmlns='com:ctl'><ctl td='CleanSt' a='$cleanedArea' s='$startCleanTimestamp' l='$lastTime' t='$trigger'/></query></iq>
 */
public class CleanStatusInfoPacket extends InfoPacket {
    public static final String INFO = "CleanSt";

    public static final String ATTRIBUT_CLEANED_AREA = "a";
    public static final String ATTRIBUTE_START_CLEAN_TIMESTAMP = "s";
    public static final String ATTRIBUTE_LAST_TIME = "l";
    public static final String ATTRIBUTE_TRIGGER = "t";


    private final String cleanedArea;
    private final String startCleanTimestamp;
    private final String lastTime;
    private final String trigger;

    @Getter
    private final CleanStatistics cleanStatistics;


    public CleanStatusInfoPacket(String innerId, XmlPullParser parser) {
        super(innerId, true, false);

        this.cleanedArea = parser.getAttributeValue(null, ATTRIBUT_CLEANED_AREA);
        this.startCleanTimestamp = parser.getAttributeValue(null, ATTRIBUTE_START_CLEAN_TIMESTAMP);
        this.lastTime = parser.getAttributeValue(null, ATTRIBUTE_LAST_TIME);
        this.trigger = parser.getAttributeValue(null, ATTRIBUTE_TRIGGER);

        this.cleanStatistics = new CleanStatistics();
        this.cleanStatistics.setCleanedArea(Integer.valueOf(cleanedArea));
        this.cleanStatistics.setStartCleanTimestamp(Long.valueOf(startCleanTimestamp));
        this.cleanStatistics.setLastTime(Long.valueOf(lastTime));
        this.cleanStatistics.setCleanStartReason(CleanStartReason.get(this.trigger));
    }

    @Override
    protected Map<String, String> getAdditionalAttributes() {
        Map<String, String> additionalAttributes = new HashMap<>();
        additionalAttributes.put(ATTRIBUT_CLEANED_AREA, cleanedArea);
        additionalAttributes.put(ATTRIBUTE_START_CLEAN_TIMESTAMP, startCleanTimestamp);
        additionalAttributes.put(ATTRIBUTE_LAST_TIME, lastTime);
        additionalAttributes.put(ATTRIBUTE_TRIGGER, trigger);

        return additionalAttributes;
    }

    @Override
    protected String getInfo() {
        return INFO;
    }
}
