/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vaccumclean.deebot.smack.packets.info;

import org.jivesoftware.smack.packet.id.StanzaIdUtil;
import org.jivesoftware.smack.util.XmlStringBuilder;
import org.jxmpp.jid.Jid;

/**
 * <iq to='$server' type='set' id='$id'><query xmlns='com:ctl'><ctl td='DeviceInfo'><MID>$mid</MID><class>$class</class><chn v='$v'/><wd>$wd</wd></ctl></query></iq>
 */
public class DeviceInfoPacket extends InfoPacket {
    public static final String INFO = "DeviceInfo";

    public static final String ELEMENT_MID = "MID";
    public static final String ELEMENT_CLASS = "class";

    public static final String ELEMENT_CHN = "chn";
    public static final String ATTRIBUTE_V = "v";

    public static final String ELEMENT_WD = "wd";

    private Jid mid;
    private String className;
    private String v;
    private String wd;

    public DeviceInfoPacket(Jid mid, String className, String v, String wd) {
        super(StanzaIdUtil.newStanzaId(), false, true);

        this.setMid(mid);
        this.setClassName(className);
        this.setV(v);
        this.setWd(wd);
    }

    public DeviceInfoPacket(Jid to, Jid mid, String className, String v, String wd) {
        super(to, false, true);

        this.setMid(mid);
        this.setClassName(className);
        this.setV(v);
        this.setWd(wd);
    }

    @Override
    protected XmlStringBuilder getAdditionalElements(XmlStringBuilder xml) {
        xml.openElement(ELEMENT_MID);
        xml.append(getMid().toString());
        xml.closeElement(ELEMENT_MID);

        xml.openElement(ELEMENT_CLASS);
        xml.append(getClassName());
        xml.closeElement(ELEMENT_CLASS);

        xml.halfOpenElement(ELEMENT_CHN);
        xml.attribute(ATTRIBUTE_V, getV());
        xml.closeEmptyElement();

        xml.openElement(ELEMENT_WD);
        xml.append(getWd());
        xml.closeElement(ELEMENT_WD);

        return xml;
    }

    @Override
    protected String getInfo() {
        return INFO;
    }

    public Jid getMid() {
        return mid;
    }

    public void setMid(Jid mid) {
        this.mid = mid;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public String getV() {
        return v;
    }

    public void setV(String v) {
        this.v = v;
    }

    public String getWd() {
        return wd;
    }

    public void setWd(String wd) {
        this.wd = wd;
    }
}
