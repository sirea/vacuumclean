/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vaccumclean.deebot.smack.packets.action;

import org.jxmpp.jid.Jid;
import org.xmlpull.v1.XmlPullParser;

import java.util.HashMap;
import java.util.Map;

/**
 * possible activities:
 * OTADOWN("otaDown"),
 * VOICEDOWN("voiceDown");
 */
public class GetProgressAction extends ActionPacket {
    public static final String ACTION = "GetProgress";

    public static final String ATTRIBUTE_ACTIVITY = "act";

    private final String activity;

    public GetProgressAction(String innerId, XmlPullParser parser) {
        super(innerId, true, false);

        this.activity = parser.getAttributeValue(null, ATTRIBUTE_ACTIVITY);
    }

    public GetProgressAction(Jid from, Jid to, String activity) {
        super(from, to, true, false);

        this.activity = activity;
    }

    @Override
    protected Map<String, String> getAdditionalAttributes() {
        Map<String, String> attributes = new HashMap<>(1);
        attributes.put(ATTRIBUTE_ACTIVITY, activity);

        return attributes;
    }

    @Override
    protected String getAction() {
        return ACTION;
    }
}
