/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vaccumclean.deebot.smack.packets.info.map;

import de.caterdev.vaccumclean.deebot.smack.packets.info.InfoPacket;
import de.caterdev.vaccumclean.deebot.smack.packets.response.map.GetMapSetResponsePacket;
import de.caterdev.vacuumclean.deebot.core.map.MapDetail;
import de.caterdev.vacuumclean.deebot.core.map.MapSetType;
import lombok.Getter;
import org.jivesoftware.smack.util.XmlStringBuilder;
import org.xmlpull.v1.XmlPullParser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <iq to='$to' type='set' id='$id'><query xmlns='com:ctl'><ctl ts='$tsValue' td='MapSet' msid='$mapSetId' tp='$mapSetType'/></query></iq>
 * <p>
 * TODO: prüfen, ob wie bei {@link de.caterdev.vaccumclean.deebot.smack.packets.response.map.GetMapSetResponsePacket} weitere Elemente enthalten sein können
 */
public class MapSetInfoPacket extends InfoPacket {
    public static final String INFO = "MapSet";

    @Getter
    private final String mapSetId;
    @Getter
    private final MapSetType mapSetType;
    @Getter
    private final List<MapDetail> mapDetails;

    public MapSetInfoPacket(String innerId, XmlPullParser parser) throws Exception {
        super(innerId, true, true, true);

        this.mapSetType = MapSetType.get(parser.getAttributeValue(null, GetMapSetResponsePacket.ATTRIBUTE_MAP_SET_TYPE));
        this.mapSetId = parser.getAttributeValue(null, GetMapSetResponsePacket.ATTRIBUTE_MAP_SET_ID);
        this.mapDetails = new ArrayList<>();

        while (parser.nextTag() == XmlPullParser.START_TAG && parser.getName().equals(GetMapSetResponsePacket.ELEMENT_MAP_DETAIL)) {
            MapDetail mapDetail = new MapDetail();
            mapDetail.setMapSetType(mapSetType);
            mapDetail.setMid(Integer.valueOf(parser.getAttributeValue(null, GetMapSetResponsePacket.ATTRIBUTE_MAP_DETAIL_ID)));
            mapDetail.setP(Integer.valueOf(parser.getAttributeValue(null, GetMapSetResponsePacket.ATTRIBUTE_MAP_DETAIL_P)));

            mapDetails.add(mapDetail);

            parser.nextTag();
        }
    }

    @Override
    protected String getInfo() {
        return INFO;
    }

    @Override
    protected Map<String, String> getAdditionalAttributes() {
        Map<String, String> attributes = new HashMap<>();
        attributes.put(GetMapSetResponsePacket.ATTRIBUTE_MAP_SET_TYPE, mapSetType.getType());
        attributes.put(GetMapSetResponsePacket.ATTRIBUTE_MAP_SET_ID, mapSetId);

        return attributes;
    }

    @Override
    protected XmlStringBuilder getAdditionalElements(XmlStringBuilder xml) {
        for (MapDetail mapDetail : mapDetails) {
            xml.halfOpenElement(GetMapSetResponsePacket.ELEMENT_MAP_DETAIL);
            xml.attribute(GetMapSetResponsePacket.ATTRIBUTE_MAP_DETAIL_ID, mapDetail.getMid());
            xml.attribute(GetMapSetResponsePacket.ATTRIBUTE_MAP_DETAIL_P, mapDetail.getP());
            xml.closeEmptyElement();
        }
        return xml;
    }
}
