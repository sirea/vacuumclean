/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vacuumclean.test.deebot.core.clean;

import de.caterdev.vaccumclean.deebot.smack.packets.info.TraceInfoPacket;
import de.caterdev.vaccumclean.deebot.smack.packets.response.GetMapModelResponsePacket;
import de.caterdev.vacuumclean.deebot.core.DataParseUtils;
import de.caterdev.vacuumclean.deebot.core.clean.CleanReplay;
import de.caterdev.vacuumclean.deebot.core.map.*;
import de.caterdev.vacuumclean.deebot.core.ui.CleanMapData;
import de.caterdev.vacuumclean.test.deebot.smack.packets.AbstractPacketTest;
import org.apache.commons.io.IOUtils;
import org.jivesoftware.smack.util.PacketParserUtils;
import org.junit.jupiter.api.Test;

import java.util.List;

public class CleanReplayTest extends AbstractPacketTest {
    final String MAPM_PACKET = "<iq xmlns='jabber:client' to='user' from='bot' id='019F4D5D0610' type='set'><query xmlns='com:ctl'><ctl id='2VPR4-49' p='50' r='8' c='8' w='100' h='100' i='1675619197' m='1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,405855736,303969177,369753284,3855924242,1295764014,1295764014,1295764014,1295764014,537589243,1905558671,1263960677,1295764014,1295764014,1295764014,1295764014,1295764014,2910585383,3735169232,3084431172,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014'/></query></iq>";

    @Test
    void testParseTrace() throws Exception {
        final String[] tracePackages = IOUtils.toString(getClass().getResourceAsStream("/tracemessages.iq"), "UTF-8").split(", ");
        MapModel mapModel = ((GetMapModelResponsePacket) PacketParserUtils.parseStanza(MAPM_PACKET)).getMapModel();
        MapInfo mapInfo = MapInfo.readMapData(getClass().getResource("/deebot.map").getFile(), mapModel);
        System.out.println(mapInfo);


        TraceInfo traceInfo = new TraceInfo();
        TraceInfoPacket packet = PacketParserUtils.parseStanza(tracePackages[0]);
        int traceId = packet.getTraceId();
        int tf = packet.getTf();
        int tt = packet.getTt();
        List<TracePoint> tracePoints = DataParseUtils.parseTracePoints(packet.getTr());
        for (TracePoint tracePoint : tracePoints) {
            float x = tracePoint.getPositionX();
            float y = tracePoint.getPositionY();

            System.out.println(tf + "->" + tt + ": (" + x + "," + y + ")");
            //System.out.print("[" + cleanMapData.getPhoneX(x + 2000) + ", " + cleanMapData.getPhoneY(y + 2000) + "],");
        }
        traceInfo.updateTrace(traceId, tf, tt, tracePoints);

        // TODO: eventuell sind die trace-Info-Punkte von der Charger-Position aus zu sehen -> GetChargerPosPacket probieren und anpassen.
        // die Trace-Daten müssten dann noch um den Faktor verringert werden, wie die unskalierte Karte dargestellt wird (vom Bot kommen ja 8000x8000 Pixel, bei einer Darstellung auf einem 800x800-Canvase wäre das denn entsprechend 1/10)
        System.out.println(traceInfo);
    }

    @Test
    void testReplayTrace() throws Exception {
        final String[] tracePackages = IOUtils.toString(getClass().getResourceAsStream("/tracemessages.iq"), "UTF-8").split(", ");

        MapModel mapModel = ((GetMapModelResponsePacket) PacketParserUtils.parseStanza(MAPM_PACKET)).getMapModel();
        MapInfo mapInfo = MapInfo.readMapData(getClass().getResource("/deebot.map").getFile(), mapModel);


        CleanMapData cleanMapData = new CleanMapData();
        cleanMapData.setMapInfo(mapInfo);
        cleanMapData.updateScale(800, 800);
        List<List<CleanMapData.Rect>> rects = cleanMapData.update();


        TraceInfo traceInfo = new TraceInfo();

        //System.out.print("var positions=[");
        for (String tracePackage : tracePackages) {
            TraceInfoPacket packet = PacketParserUtils.parseStanza(tracePackage);
            int traceId = packet.getTraceId();
            int tf = packet.getTf();
            int tt = packet.getTt();
            List<TracePoint> tracePoints = DataParseUtils.parseTracePoints(packet.getTr());
            for (TracePoint tracePoint : tracePoints) {
                float x = tracePoint.getPositionX();
                float y = tracePoint.getPositionY();
                System.out.println(tf + "->" + tt + ": (" + x + "," + y + ")" + " --- (" + cleanMapData.getPhoneX(x) + ", " + cleanMapData.getPhoneY(y) + ")" + " --- (" + tracePoint.getScaledX(cleanMapData) + ", " + tracePoint.getScaledY(cleanMapData) + ")");
                //System.out.print("[" + cleanMapData.getPhoneX(x + 2000) + ", " + cleanMapData.getPhoneY(y + 2000) + "],");
            }
            traceInfo.updateTrace(traceId, tf, tt, tracePoints);
        }
        //System.out.println("]");


        System.out.println(traceInfo);
        System.out.println(mapInfo);
        System.out.println(mapModel);

        cleanMapData.setChargerPosition(new Position(106, 169, -670));
        System.out.println(cleanMapData.getChargerPosition());

        CleanReplay cleanReplay = new CleanReplay(cleanMapData, mapInfo, traceInfo);
        cleanReplay.writeUnscaledHTML("target/cleaningtrace.html");
    }
}
