/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vacuumclean.test.deebot.smack.packets.info;

import de.caterdev.vaccumclean.deebot.smack.packets.info.ErrorInfoPacket;
import de.caterdev.vaccumclean.deebot.smack.packets.info.InfoPacket;
import de.caterdev.vaccumclean.deebot.smack.packets.info.bot.DustCaseInfoPacket;
import de.caterdev.vacuumclean.test.deebot.smack.packets.AbstractPacketTest;
import org.jivesoftware.smack.util.PacketParserUtils;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class InfoPacketParserTest extends AbstractPacketTest {
    /**
     * Creates an {@link InfoPacket} based on the given packet string and checks values of to, id, innerId Attributes.
     *
     * @param packetString
     * @param <T>
     * @return
     * @throws Exception
     */
    protected <T extends InfoPacket> T parseInfoPacket(String packetString) throws Exception {
        T packet = PacketParserUtils.parseStanza(packetString);

        assertEquals(to, packet.getTo().asUnescapedString());
        assertEquals(id, packet.getStanzaId());
        assertEquals(innerId, packet.getInnerId());
        assertEquals(packetString, packet.toXML(null).toString());

        return packet;
    }

    @Test
    void testParseDustCaseInfoPacket() throws Exception {
        final String status = "0";
        final String packetString = String.format("<iq xmlns='jabber:client' to='%s' id='%s' type='set'><query xmlns='com:ctl'><ctl ts='%s' td='DustCaseST' st='%s'/></query></iq>", to, id, innerId, status);

        DustCaseInfoPacket packet = parseInfoPacket(packetString);

        assertEquals(status, packet.getStatus());
    }

    @Test
    void testParseErrorInfoPacket() throws Exception {
        final String oldError = "";
        final String newError = "102";
        final String packetString = String.format("<iq xmlns='jabber:client' to='%s' id='%s' type='set'><query xmlns='com:ctl'><ctl ts='%s' td='errors' new='%s' old='%s'/></query></iq>", to, id, innerId, newError, oldError);

        ErrorInfoPacket packet = parseInfoPacket(packetString);
        assertEquals(oldError, packet.getOldError());
        assertEquals(newError, packet.getNewError());
    }
}
