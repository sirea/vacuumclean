/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vacuumclean.test.deebot.smack.packets.info;

import de.caterdev.vaccumclean.deebot.smack.packets.info.map.MapSetInfoPacket;
import de.caterdev.vacuumclean.deebot.core.map.MapSetType;
import de.caterdev.vacuumclean.test.deebot.smack.packets.AbstractPacketTest;
import org.jivesoftware.smack.util.PacketParserUtils;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class MapSetInfoPacketTest extends AbstractPacketTest {
    public static final String mapSetId = "10";
    public static final String mapSetType = MapSetType.SpotArea.getType();

    public static final String PACKET = String.format("<iq xmlns='jabber:client' to='%s' id='%s' type='set'><query xmlns='com:ctl'><ctl ts='%s' td='MapSet' msid='%s' tp='%s'><m mid='0' p='1'/><m mid='1' p='1'/><m mid='2' p='1'/><m mid='3' p='1'/><m mid='4' p='1'/><m mid='5' p='1'/></ctl></query></iq>", to, id, innerId, mapSetId, mapSetType);

    @Test
    void testParsePacket() throws Exception {
        MapSetInfoPacket packet = PacketParserUtils.parseStanza(PACKET);

        assertEquals(to, packet.getTo().asUnescapedString());
        assertEquals(id, packet.getStanzaId());
        assertEquals(innerId, packet.getInnerId());
        assertEquals(mapSetId, packet.getMapSetId());
        assertEquals(mapSetType, packet.getMapSetType().getType());

        assertEquals(PACKET,packet.toXML(null).toString());
    }
}
