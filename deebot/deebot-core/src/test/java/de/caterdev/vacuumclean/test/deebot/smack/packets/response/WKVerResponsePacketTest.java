/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vacuumclean.test.deebot.smack.packets.response;

import de.caterdev.vaccumclean.deebot.smack.packets.response.WKVerResponsePacket;
import de.caterdev.vacuumclean.test.deebot.smack.packets.AbstractPacketTest;
import org.jivesoftware.smack.packet.IQ;
import org.jivesoftware.smack.packet.id.StanzaIdUtil;
import org.jivesoftware.smack.util.PacketParserUtils;
import org.junit.jupiter.api.Test;
import org.jxmpp.jid.impl.JidCreate;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class WKVerResponsePacketTest extends AbstractPacketTest {
    public static final String id = StanzaIdUtil.newStanzaId();
    public static final String innerId = StanzaIdUtil.newStanzaId();
    public static final String from = "from";
    public static final String to = "to";
    public static final String ver = "0.15.2";

    public static final String PACKET = String.format("<iq xmlns='jabber:client' to='%s' id='%s' type='set'><query xmlns='com:ctl'><ctl id='%s' ret='ok' td='WKVer'><ver name='FW'>%s</ver></ctl></query></iq>", to, id, innerId, ver);

    @Test
    void testParsePacket() throws Exception {
        WKVerResponsePacket packet = PacketParserUtils.parseStanza(PACKET);

        assertNotNull(packet);
        assertEquals(id, packet.getStanzaId());
        assertEquals(to, packet.getTo().asUnescapedString());
        assertEquals(innerId, packet.getInnerId());
        assertEquals(ver, packet.getFwVer());
    }

    @Test
    void testCreatePacket() throws Exception {
        WKVerResponsePacket packet = new WKVerResponsePacket(JidCreate.from(to), ver);

        packet.setStanzaId(id);
        packet.setInnerId(innerId);

        assertEquals(id, packet.getStanzaId());
        assertEquals(to, packet.getTo().asUnescapedString());
        assertEquals(innerId, packet.getInnerId());
        assertEquals(ver, packet.getFwVer());
        assertEquals(IQ.Type.set, packet.getType());

        assertEquals(PACKET, packet.toXML(null).toString());
    }
}
