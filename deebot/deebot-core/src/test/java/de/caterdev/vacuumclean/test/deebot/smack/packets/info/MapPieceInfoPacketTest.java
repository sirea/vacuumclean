/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vacuumclean.test.deebot.smack.packets.info;

import de.caterdev.vaccumclean.deebot.smack.packets.info.MapPInfoPacket;
import de.caterdev.vaccumclean.deebot.smack.packets.response.GetMapModelResponsePacket;
import de.caterdev.vacuumclean.deebot.core.map.MapInfo;
import de.caterdev.vacuumclean.deebot.core.map.MapModel;
import de.caterdev.vacuumclean.deebot.core.map.MapPiece;
import de.caterdev.vacuumclean.deebot.core.ui.CleanMapData;
import de.caterdev.vacuumclean.test.deebot.smack.packets.AbstractPacketTest;
import org.jivesoftware.smack.util.PacketParserUtils;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class MapPieceInfoPacketTest extends AbstractPacketTest {
    public static final String MAPM_PACKET = "<iq xmlns='jabber:client' to='user' from='bot' id='019F4D5D0610' type='set'><query xmlns='com:ctl'><ctl id='2VPR4-49' p='50' r='8' c='8' w='100' h='100' i='1675619197' m='1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,405855736,303969177,369753284,3855924242,1295764014,1295764014,1295764014,1295764014,537589243,1905558671,1263960677,1295764014,1295764014,1295764014,1295764014,1295764014,2910585383,3735169232,3084431172,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014,1295764014'/></query></iq>";
    public static final String PACKET = "<iq to='user' type='set' id='0521E810DEA0'><query xmlns='com:ctl'><ctl td='MapP' i='1675619197' pid='29' p='XQAABAAQJwAAAADukkeodhuFu/rAYHwpotYmse5wT4ZFzEUwk24lmodZJYapV4wgct86Mf/RIeoUBr9AgHzovH5DnzPWqRSfTUybgFJPmDge3Ru7ZkSMhXTUYAXQu137Rvoq75RaegOHazUiyFzLhuzp6P/YLQi3zHM8Aq84hoXSN+Gbxz148MzfAvR5<iq to='username23@ecouser.net/resource' type='set' id='088CF9DB0D40'><query xmlns='com:ctl'><ctl td='MapP' i='1675619197' pid='20' p='XQAABAAQJwAAAABv/f//o7f/Rz5IFXI5YVG4S6bS+tod4bdZxu3oFqsMlxN8RtklnfYEchqKb3NzgAPhqUuGRoK3yzQ5Qv+1clSQwr5EEeeAllgPpMXFQROIh0Ko9gFtj2hA3/iIhkA36YibGgEiM7IhrZdQTjjRiEsJ7koyit8P1MaoeqWybpBzTIfzLykrKEHij53ueICBwRa+Xlie7aaIDIWlNpFV0ZUhSXQtlr5AjSvyhjH97UfVG820WfObNbj73FnSJQ1CfS94VPlJPQYYVuap0+OwezYwp5TNybaAcmgFErAGwP0D/kjpBSklpwYnwXIAPlyB7AJygiipKxSzDkfzFQgSZPg5zFD3jeto5onrk3TnQNjhFUMtVciLcZ/JOpRYjkZNzmrRkDPJTJF5WI/YPldTMTmjd6otPPwC17TqBr8gI/HSx6A2o2eGyrgCea/8z45byCQ6N2fmhOSQghqAQqcqPQiLuoLaWLbaUx5wgdQj6sEfJJR7e0yv4r6z1rXXxU75fZH2q10ovsA7GGzJjWprlX+TcnOzLSIxp4OLhaUlLxxGpxaT8MJAjM3SfIe1Uik7Ho8G9k2xM3Xn9GUkck2t5iJbzALBVfXTfzCoZ5WXARm9dTkGHU2y9dgTc+/oEUCHSKaTpXADhcUkvtwroiDRIjTBFZ1n6A5oPBEJSrYwkKPw8IyZkh5HmY4efDq9Huur+0RevO3cX+Bj8cZzGM9vSSXH1kCFEA1lRRXy0NqOHBy2oElCIAflwR45Ice7fzDemzZAHO7QtD/6ym62BfuGKJzCtA=='/></query></iq>";

    public static byte[] readMapFile(String filename, int paramInt) throws Exception {
        byte[] arrayOfByte = new byte[paramInt];
        File file = new File(filename);
        if (!file.exists())
            file.createNewFile();
        FileInputStream fileInputStream = new FileInputStream(file);
        if (paramInt != fileInputStream.available()) {
            delFile(filename);
            writeFile(filename, arrayOfByte);
        } else {
            fileInputStream.read(arrayOfByte);
        }
        fileInputStream.close();
        return arrayOfByte;
    }

    private static boolean delFile(String paramString) throws Exception {
        return Files.deleteIfExists(Paths.get(paramString));
    }

    public static void writeFile(String filename, byte[] paramArrayOfByte) throws Exception {
        FileOutputStream fileOutputStream = new FileOutputStream(filename);
        fileOutputStream.write(paramArrayOfByte);
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Test
    void testParseSingleMapPInfoPacket() throws Exception {
        String stanza = "<iq xmlns='jabber:client' to='user' from='bot' id='0B50D30B30B0' type='set'><query xmlns='com:ctl'><ctl td='MapP' p='XQAABAAQJwAAAABujkfBG5HG9K1n306qCGolWRQVS2YrXse4YFK2cJS6VMnLL0cSmphjh3diJvqxetTJKCkmWvM2SVFeDQpz4BnjLPv1P8fVo2hzr8KVtL+5kZGZgas3wy93N9kBOkJdQqx6pLkkT2yOuorIIb5q/spcZf03j7f22DtObGqPfeTgnsTcsKlhR9+xU/fGvb148PGQXorrzjRR+5FfSxWTWxsZs//f7JIM+snR+ZOOftXE5Ww14dxCMAI4OqQr8wz8RwqRmfra1H1ciFn9EI/EvpCwgYkKnxqxqO4AfRbcyqQByWHkeXg3xAQIXTDPZZCjTQTTPUOIqjlwUnJWbnOfItdU90LxSj+Or6DGV6fLpwoZfU/DgkS0/oo5LEt+qeIs9oQ1/hY4OmvawW0AUBkg5AScgx8isyNp+JI=' i='1675619197' pid='27'/></query></iq>";
        MapPiece mapPiece = ((MapPInfoPacket) PacketParserUtils.parseStanza(stanza)).getMapPiece();
        MapModel mapModel = ((GetMapModelResponsePacket) PacketParserUtils.parseStanza(MAPM_PACKET)).getMapModel();

        System.out.println(mapModel);
        System.out.println(mapPiece);


        MapInfo mapInfo = new MapInfo(mapModel);
        //mapInfo.setMapId(mapM.getMapId());
        //mapInfo.init(mapM.getRowGrid(), mapM.getColumnGrid(), mapM.getRowPiece(), mapM.getColumnPiece());
        //mapInfo.setPixelWidth(mapM.getPixelWidth());
        //mapInfo.setBoxTopLeft(mapM.getBoxTopLeft());
        //mapInfo.setBoxBottomRight(mapM.getBoxBottomRight());

        if (0 != mapInfo.getChecksum(mapPiece.getPieceId()) && mapInfo.getChecksum(mapPiece.getPieceId()) != mapPiece.getChecksum()) {
            mapInfo.updateMapBuffer(mapPiece);
        }

        CleanMapData cleanMapData = new CleanMapData();
        cleanMapData.setMapInfo(mapInfo);
        cleanMapData.updateScale(800, 600);
        System.out.println(cleanMapData);
        List<List<CleanMapData.Rect>> rects = cleanMapData.update();
        System.out.println(rects);

        for (int i = 0; i < rects.size(); i++) {
            System.out.println(i + ": " + rects.get(i).size());
        }

        System.out.println("mainContext.fillStyle = '#AAAAAA';");
        for (CleanMapData.Rect rect : rects.get(0)) {
            System.out.println("mainContext.fillRect(" + rect.getLeft() + "," + rect.getTop() + ",4,4);");
        }
        System.out.println("mainContext.fillStyle = '#000000';");
        for (CleanMapData.Rect rect : rects.get(1)) {
            System.out.println("mainContext.fillRect(" + rect.getLeft() + "," + rect.getTop() + ",4,4);");
        }
        System.out.println("mainContext.fillStyle = '#DDDDDD';");
        for (CleanMapData.Rect rect : rects.get(2)) {
            System.out.println("mainContext.fillRect(" + rect.getLeft() + "," + rect.getTop() + ",4,4);");
        }
    }

    /*
    @Test
    void testParsePacket() throws Exception {
        MapM mapM = ((GetMapMResponsePacket) PacketParserUtils.parseStanza(MAPM_PACKET)).getMapM();
        System.out.println("mapm: " + mapM + ", #crc: " + mapM.getCrc().length);

        MapInfo mapInfo = new MapInfo();
        mapInfo.setMapId(mapM.getMapId());
        mapInfo.init(mapM.getRowGrid(), mapM.getColumnGrid(), mapM.getRowPiece(), mapM.getColumnPiece());
        mapInfo.setPixelWidth(mapM.getPixelWidth());
        mapInfo.setBoxTopLeft(mapM.getBoxTopLeft());
        mapInfo.setBoxBottomRight(mapM.getBoxBottomRight());

        System.out.println("mapinfo: " + mapInfo);

        CRC32 crc32 = new CRC32();
        crc32.update(new byte[mapM.getRowGrid() * mapM.getColumnGrid()]);
        long l = crc32.getValue();
        System.out.println("l: " + l);

        for (byte b = 0; b < mapM.getCrc().length; b++) {
            if (mapM.getCrc()[b] != l) {
                System.out.println("match bei " + mapM.getCrc()[b]);
                byte[] bytes = readMapFile("robotMap.bin", mapM.getRowGrid() * mapM.getColumnGrid() * mapM.getRowPiece() * mapM.getColumnPiece());
                mapInfo.updateMapBuffer(bytes);
            }
            if (b == mapM.getCrc().length - 1) {
                System.out.println("length match bei " + b);
                // delete map file
            }
        }

        System.out.println(mapInfo);

        String[] mapPPackages = IOUtils.toString(getClass().getResourceAsStream("/mappmessages.iq"), "UTF-8").split(",");
        System.out.println("#mapp-packages: " + mapPPackages.length);

        for (String mapPPackage : mapPPackages) {
            MapPInfoPacket packet = PacketParserUtils.parseStanza(mapPPackage);

            System.out.println(packet.getMapId() + ", " + packet.getPid() + ": " + packet.getData());

            int i = Integer.valueOf(packet.getPid());
            byte[] arrayOfBytes = DataParseUtils.decode7zData(packet.getData());
            crc32 = new CRC32();
            crc32.update(arrayOfBytes);
            l = mapInfo.getPieceCrc32(i);

            if (0L != l && l != crc32.getValue()) {
                //System.out.println("updating " + i + " with " + Arrays.toString(arrayOfBytes));
                mapInfo.updateMapBuffer(i, arrayOfBytes);
                writeFile("robotMap.bin", mapInfo.getBufferAsOne());
            }
        }
        System.out.println(mapInfo.getMapId());

        int j;
        for (int i = j = 0; i < mapInfo.getWidth(); ++i) {
            int n;
            for (int k = 0; k < mapInfo.getHeight(); ++k, j = n) {
                n = j;
                if (mapInfo.getBuffer()[i][k] == 2) {
                    n = j + 1;
                }
            }
        }
        final StringBuilder sb = new StringBuilder();
        sb.append("buffer=2 path3.size=");
        sb.append(j);
        System.out.printf("refreshmap %s\n", sb.toString());

        CleanMapData cleanMapData = new CleanMapData();
        cleanMapData.setMapInfo(mapInfo);

        calculateScale(mapInfo, cleanMapData);
        List<List<Rect>> rects = update(cleanMapData);

        for (int i = 0; i < rects.size(); i++) {
            System.out.println(i + ": " + rects.get(i).size());
        }

        List<Rect> e = rects.get(0);
        System.out.println("mainContext.fillStyle = '#FFBB00';");
        for (Rect rect : e) {
            //System.out.println("mainContext.fillRect(" + rect.left + "," + rect.top + ",4,4);");
        }
        System.out.println("mainContext.fillStyle = '#ABCABC';");
        for (Rect rect : rects.get(1)) {
            //System.out.println("mainContext.fillRect(" + rect.left + "," + rect.top + ",4,4);");
        }
        System.out.println("mainContext.fillStyle = '#CBACBA';");
        for (Rect rect : rects.get(2)) {
            //System.out.println("mainContext.fillRect(" + rect.left + "," + rect.top + ",4,4);");
        }
    }

     */



}
