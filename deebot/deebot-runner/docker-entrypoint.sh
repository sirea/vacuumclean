#!/bin/sh

set -e

echo "Importing certificate (skip if it was already imported)..."
keytool -list -alias vacuumcleaner -keystore $JAVA_HOME/lib/security/cacerts -storepass changeit > /dev/null ||\
  echo yes | keytool -import -file vacuumcleaner.cer -alias vacuumcleaner -keystore $JAVA_HOME/lib/security/cacerts -storepass changeit

echo "Starting deebot-runner..."
java -DXMPP_HOST=$XMPP_HOST -DXMPP_PORT=$XMPP_PORT -jar deebot-runner-0.1-jar-with-dependencies.jar