/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vacuumclean.runner;

import de.caterdev.vacuumclean.deebot.server.DeebotRobotServer;
import de.caterdev.vacuumclean.deebot.server.DeebotXMPPServer;
import org.apache.commons.cli.*;

import java.net.InetAddress;
import java.net.InetSocketAddress;

public class VacuumcleanRunner {
    private static final Options options = new Options();
    private static final Option addressOption = new Option("a", "address", true, "server address");

    static {
        options.addOption(addressOption);
    }

    public static void main(String[] args) throws Exception {
        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = parser.parse(options, args);

        InetAddress serverAddress;
        if (cmd.hasOption(addressOption.getOpt())) {
            serverAddress = InetAddress.getByName(cmd.getOptionValue(addressOption.getOpt()));
        } else {
            serverAddress = InetAddress.getLocalHost();
        }

        DeebotXMPPServer xmppServer = new DeebotXMPPServer(new InetSocketAddress(serverAddress, DeebotXMPPServer.DEFAULT_PORT));
        DeebotRobotServer robotServer = new DeebotRobotServer(new InetSocketAddress(serverAddress, DeebotRobotServer.DEFAULT_PORT));


        new Thread(xmppServer).start();
        new Thread(robotServer).start();
    }
}
