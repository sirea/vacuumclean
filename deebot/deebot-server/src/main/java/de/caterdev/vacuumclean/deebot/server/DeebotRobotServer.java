/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vacuumclean.deebot.server;

import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;
import lombok.extern.java.Log;
import org.json.JSONObject;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;

@Log
public class DeebotRobotServer implements Runnable {
    static final String ENV_XMPP_HOST = "XMPP_HOST";
    static final String ENV_XMPP_PORT = "XMPP_PORT";

    public static final String LOG_MESSAGE_STARTING_SERVER = "Starting %s server at %s\n";
    public static final String LOG_MESSAGE_INCOMING_REQUEST = "Incoming request from %s to %s\n";
    public static final String LOG_MESSAGE_HEADERS = "Headers: %s\n";
    public static final String LOG_MESSAGE_SYSTEM_PROPERTY_NOT_FOUND = "\"System property '%s' was not found or was empty. Using default value: '%s'\"";

    public static final int DEFAULT_PORT = 8007;

    private final InetSocketAddress address;

    public DeebotRobotServer(InetSocketAddress address) {
        this.address = address;
    }

    @Override
    public void run() {
        try {
            HttpServer server = HttpServer.create(address, 0);

            server.createContext("/", new MyHandler());
            server.setExecutor(null);
            log.info(String.format(LOG_MESSAGE_STARTING_SERVER, getClass().getSimpleName(), server.getAddress()));

            server.start();
        } catch (Exception e) {
            log.severe(e.toString());
        }
    }

    static class MyHandler implements HttpHandler {
        public void handle(HttpExchange httpExchange) throws IOException {
            log.info(String.format(LOG_MESSAGE_INCOMING_REQUEST, httpExchange.getRemoteAddress(), httpExchange.getRequestURI()));
            log.fine(String.format(LOG_MESSAGE_HEADERS, httpExchange.getRequestHeaders().entrySet()));

            String xmppIpAddress = getSystemPropertyOrDefault(ENV_XMPP_HOST, httpExchange.getLocalAddress().getHostString());
            String xmppPort = getSystemPropertyOrDefault(ENV_XMPP_PORT, String.valueOf(DeebotXMPPServer.DEFAULT_PORT));

            JSONObject result = new JSONObject();
            result.put("ip", xmppIpAddress);
            result.put("port", Integer.valueOf(xmppPort));
            result.put("result", "ok");
            log.info(result.toString());

            httpExchange.sendResponseHeaders(200, result.toString().getBytes().length);
            OutputStream output = httpExchange.getResponseBody();
            output.write(result.toString().getBytes());
            output.close();
        }

        private String getSystemPropertyOrDefault(String propertyKey, String defaultValue) {
            String value = System.getProperty(propertyKey);

            if (value == null || value.isEmpty()) {
                value = defaultValue;
                log.fine(String.format(LOG_MESSAGE_SYSTEM_PROPERTY_NOT_FOUND, propertyKey, value));
            }

            return value;
        }
    }
}
