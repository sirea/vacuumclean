/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vacuumclean.deebot.server;

import org.json.JSONObject;
import org.junit.jupiter.api.Test;

import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.URL;
import java.util.Scanner;

import static de.caterdev.vacuumclean.deebot.server.DeebotRobotServer.ENV_XMPP_HOST;
import static de.caterdev.vacuumclean.deebot.server.DeebotRobotServer.ENV_XMPP_PORT;
import static org.junit.jupiter.api.Assertions.*;

public class DeebotRobotServerTest {
    @Test
    void testReturnsXMPPServerAddress() throws Exception {
        InetAddress serverAddress = InetAddress.getLocalHost();
        DeebotRobotServer server = new DeebotRobotServer(new InetSocketAddress(serverAddress, DeebotRobotServer.DEFAULT_PORT));
        server.run();

        URL url = new URL("http", serverAddress.getHostAddress(), DeebotRobotServer.DEFAULT_PORT, "/lookup.do");
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setDoOutput(true);
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-Type", "application/json");

        BufferedWriter httpRequestBodyWriter = new BufferedWriter(new OutputStreamWriter(con.getOutputStream()));
        httpRequestBodyWriter.write("{'todo':'FindBest','service':'EcoMsgNew'}");
        httpRequestBodyWriter.close();

        assertEquals(200, con.getResponseCode());

        String response = new Scanner(con.getInputStream()).nextLine();
        assertNotNull(response);

        JSONObject json = new JSONObject(response);
        assertTrue(json.has("ip"));
        assertTrue(json.has("port"));
        assertTrue(json.has("result"));

        con.disconnect();
    }

    @Test
    void xmpp_host_and_xmpp_port_is_retrieved_from_system_properties() throws Exception {
        String xmppHost = "dummyHost";
        Integer xmppPort = 123;

        System.setProperty(ENV_XMPP_HOST, xmppHost);
        System.setProperty(ENV_XMPP_PORT, xmppPort.toString());

        InetAddress serverAddress = InetAddress.getLocalHost();
        DeebotRobotServer server = new DeebotRobotServer(new InetSocketAddress(serverAddress, DeebotRobotServer.DEFAULT_PORT));
        server.run();

        URL url = new URL("http", serverAddress.getHostAddress(), DeebotRobotServer.DEFAULT_PORT, "/lookup.do");
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setDoOutput(true);
        con.setRequestMethod("POST");
        con.setRequestProperty("Content-Type", "application/json");

        BufferedWriter httpRequestBodyWriter = new BufferedWriter(new OutputStreamWriter(con.getOutputStream()));
        httpRequestBodyWriter.write("{'todo':'FindBest','service':'EcoMsgNew'}");
        httpRequestBodyWriter.close();

        assertEquals(200, con.getResponseCode());

        String response = new Scanner(con.getInputStream()).nextLine();
        assertNotNull(response);

        JSONObject json = new JSONObject(response);
        assertEquals(xmppHost, json.getString("ip"));
        assertEquals(xmppPort, json.getInt("port"));
        con.disconnect();
    }
}