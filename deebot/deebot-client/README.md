# Deebot Client

## Startup
The client is startet by running AppClient.kt. You may have to alter the server address in `val connection = Deebot930ConnectionManager().initConnection("localhost")`. After startup, the client listens on port 8080 and can be controlled using HTTP-request, i.e. you can use your browser to send commands view `http://serverAddress:8080/{command}`

Visit `http://serverAddress:8080/` for an overview of commands.

## Functions
### Restore Map
* command: restoreMap

If your robot already has a map, you can retrieve this map using the command `restoreMap`. The client will pull all map pieces and update the pieces that have a different checksum.

### Compare Checksums
* command: checksums

After the map was retrieved from the bot, you should call `checksums` to compare local and remote checksums. This displays the checksum of each piece as received from the bot and the local checksum. There should be no messages that checksums do not match.

### Display Map
* command: map/{width}/{height}

This command returns a website containing the map scaled to the given dimensions.

### Clean
* command: clean

The robot starts auto cleaning with default settings (speed=standard, act=s) `[TODO: determine meaning of act]`

### Charge
* command: charge

The robot returns to the charging station.

### Battery Info
* command: battery

Displays a website with the battery status in percentage.

### Charge Type
* command: chargeType

Displays a website with the current charge state.

### Autoreport
* command: autoreport/{type}

(De-)activates auto reporting. When auto reporting is activated and the robot moves, it continuously sends map data to the client. Incoming map data updated existing map data.