/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vacuumclean.deebot.client

import de.caterdev.vaccumclean.deebot.smack.packets.Query
import de.caterdev.vaccumclean.deebot.smack.packets.action.*
import de.caterdev.vaccumclean.deebot.smack.packets.action.map.GetMapSetPacket
import de.caterdev.vaccumclean.deebot.smack.packets.action.map.PullMPacket
import de.caterdev.vaccumclean.deebot.smack.packets.action.sched.GetSchedPacket
import de.caterdev.vacuumclean.deebot.core.clean.CleanStatistics
import de.caterdev.vacuumclean.deebot.core.map.MapSetType
import io.ktor.application.call
import io.ktor.http.ContentType
import io.ktor.http.content.resource
import io.ktor.response.respondText
import io.ktor.routing.get
import io.ktor.routing.routing
import io.ktor.server.engine.embeddedServer
import io.ktor.server.netty.Netty
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.jxmpp.jid.Jid
import org.jxmpp.jid.impl.JidCreate
import java.time.Instant

fun main(args: Array<String>) {
    var serverAddress = "localhost"
    if (args.size == 1) {
        serverAddress = args[0]
    }

    val ctls: MutableMap<String, ActionPacket<in Query>> = HashMap()
    val connection = Deebot930ConnectionManager().initConnection(serverAddress)
    val robot = Bot();
    val iqManager = Deebot930IQManager(ctls, connection, robot = robot)


    Thread(PingRunner(connection)).start()

    val bots: List<Jid> = iqManager.authoriseUserWithBot()

    val user = connection.user
    val bot = bots[0]
    val autoReportRunner = AutoReportRunner("0", iqManager, user, bot)
    GlobalScope.launch { autoReportRunner.autoreport() }



    embeddedServer(Netty, 8080) {
        routing {
            resource("/", "index.html")
            get("/time") {
                call.respondText(iqManager.getTime(user, bot), ContentType.Text.Html)
            }
            get("/users") {
                call.respondText(iqManager.getUserInfo(user, bot).toString(), ContentType.Text.Html)
            }
            get("/battery") {
                call.respondText(iqManager.getBatteryInfo(user, bot), ContentType.Text.Html)
            }
            get("/chargeType") {
                call.respondText(iqManager.getChargeState(user, bot).name, ContentType.Text.Html)
            }
            get("/close") {
                connection.disconnect()
            }
            get("/autoreport/{report}") {
                val autoreport = call.parameters["report"]!!

                call.respondText(iqManager.setMapAutoReport(user, bot, autoreport).name, ContentType.Text.Html)
                autoReportRunner.autoreport = autoreport;
            }
            get("/delUser") {
                connection.sendIqRequestAsync(DelUserPacket(user, bot, JidCreate.from("username@ecouser.net")))
            }
            get("/getACS") {
                connection.sendIqRequestAsync(GetACSPacket(user, bot))
            }
            get("/getAccessControl") {
                connection.sendIqRequestAsync(GetAccessControlAction(user, bot))
            }
            get("/getDCStatus") {
                connection.sendIqRequestAsync(GetDCStatusPacket(user, bot))
            }
            get("/action/{action}") {
                connection.sendIqRequestAsync(GenericActionPacket(user, bot, call.parameters["action"]))
            }
            get("getWKVer") {
                connection.sendIqRequestAsync(GetWKVerPacket(user, bot))
            }
            get("/getChargerPos") {
                val result = iqManager.getChargerPos(user, bot)
                val x = robot.cleanMap.getPhoneX(result.x)
                val y = robot.cleanMap.getPhoneY(result.y)
                call.respondText(result.toString() + ", phoneX: " + x + ", phoneY: " + y)
            }
            get("/getPos") {
                val result = iqManager.getPos(user, bot)
                val x = robot.cleanMap.getPhoneX(result.x)
                val y = robot.cleanMap.getPhoneY(result.y)

                call.respondText(result.toString() + ", phoneX: " + x + ", phoneY: " + y)
            }
            get("/charge") {
                call.respondText(iqManager.charge(user, bot, ChargePacket.ChargeAction.go).name, ContentType.Text.Html)
            }
            get("/saveMap") {
                robot.cleanMap.mapInfo.writeMapData("deebot.map")
            }
            get("/map/{width}/{height}") {
                var width = call.parameters["width"]?.toFloat()
                var height = call.parameters["height"]?.toFloat();


                if (null == width || null == height) {
                    width = 800F
                    height = 600F
                }

                robot.cleanMap.updateScale(width, height)
                val rects = robot.cleanMap.update()

                var html = "<!DOCTYPE html><html><head><title>Simple Canvas Example</title><style>canvas {border: 3px #CCC solid;}  </style></head><body><div id=\"container\"><canvas id=\"myCanvas\" height='" + height + "' width='" + width + "'></canvas></div><script>var mainCanvas = document.querySelector(\"#myCanvas\");var mainContext = mainCanvas.getContext(\"2d\");var canvasWidth = mainCanvas.width;var canvasHeight = mainCanvas.height;function drawCircle() {mainContext.clearRect(0, 0, canvasWidth, canvasHeight);";


                html += "mainContext.fillStyle='#AAAAAA';";

                for (rect in rects.get(0)) {
                    html += "mainContext.fillRect(" + rect.getLeft() + "," + rect.getTop() + ",4,4);"
                }
                html += "mainContext.fillStyle = '#000000';"
                for (rect in rects.get(1)) {
                    html += "mainContext.fillRect(" + rect.getLeft() + "," + rect.getTop() + ",4,4);"
                }
                html += "mainContext.fillStyle = '#DDDDDD';"
                for (rect in rects.get(2)) {
                    html += "mainContext.fillRect(" + rect.getLeft() + "," + rect.getTop() + ",4,4);"
                }

                html += "mainContext.fillStyle = '#FF00FF';";
                html += "mainContext.arc(" + robot.cleanMap.getPhoneX(robot.cleanMap.botPosition.originalX) + ", " + robot.cleanMap.getPhoneY(robot.cleanMap.botPosition.originalY) + ", 10, 0, 2*Math.PI);"
                html += "mainContext.fill();"

                html += "mainContext.fillStyle = '#FF00FF';";
                //html += "mainContext.rotate(" + robot.cleanMap.chargerPosition.angle + "* Math.PI / 180);";
                html += "mainContext.fillRect(" + (robot.cleanMap.getPhoneX(robot.cleanMap.chargerPosition.originalX) - 10) + ", " + robot.cleanMap.getPhoneY(robot.cleanMap.chargerPosition.originalY) + ", 20, 20);"
                html += "mainContext.closePath();}drawCircle();</script></body></html>"

                call.respondText(html, ContentType.Text.Html)
            }
            get("/traces") {
                call.respondText(robot.mappedTraces.entries.toString())
            }
            get("/pullMP/{pid}") {
                call.respondText(iqManager.pullMP(user, bot, call.parameters["pid"]!!.toInt()))
            }
            get("/getMapModel") {
                call.respondText(iqManager.getMapModel(user, bot))
            }
            get("/restoreMap") {
                call.respondText(iqManager.restoreMap(user, bot))
            }
            get("/checksums") {
                call.respondText(iqManager.compareChecksums(user, bot))
            }
            get("/trace/{traceId}/{traceFrom}/{traceTo}") {
                call.respondText(iqManager.getTraceData(user, bot, call.parameters["traceId"]!!, call.parameters["traceFrom"]!!, call.parameters["traceTo"]!!))
            }
            get("/mapSetVW") {
                connection.sendIqRequestAsync(GetMapSetPacket(user, bot, MapSetType.VirtualWall))
            }
            get("/mapSetCP") {
                connection.sendIqRequestAsync(GetMapSetPacket(user, bot, MapSetType.Carpet))
            }
            get("/mapSetSA") {
                connection.sendIqRequestAsync(GetMapSetPacket(user, bot, MapSetType.SpotArea))
            }
            get("/cleanLogs/{count}") {
                val cleansum = iqManager.getCleanSum(user, bot)
                val entries = iqManager.getCleanLogs(user, bot, call.parameters["count"]!!)

                val html = StringBuilder()
                html.append("<html>\n<body>\n");
                html.append("overall cleaning sum\n")
                html.append("<ul>\n")
                html.append("<li>cleaned area: ").append(cleansum.cleandArea).append("</li>\n")
                html.append("<li>duration: ").append(cleansum.duration / 60).append("</li>\n")
                html.append("<li>count: ").append(cleansum.count).append("</li>\n")
                html.append("</ul>\n")
                html.append("<table border='1'>\n");
                html.append("<tr><th>start</th><th>area</th><th>duration</th><th>start reason</th><th>stop reason</th></tr>\n")

                entries.forEach { cleanStatistics: CleanStatistics ->
                    html.append("<tr>\n")
                    html.append("<td>").append(Instant.ofEpochSecond(cleanStatistics.startCleanTimestamp)).append("</td>\n")
                    html.append("<td>").append(cleanStatistics.cleanedArea).append("</td>\n")
                    html.append("<td>").append(cleanStatistics.lastTime / 60).append("</td>\n")
                    html.append("<td>").append(cleanStatistics.cleanStartReason).append("</td>\n")
                    html.append("<td>").append(cleanStatistics.cleanStopReason).append("</td>\n")
                    html.append("</tr>\n")
                }
                html.append("</table>\n")
                html.append("</body>\n</html>")

                call.respondText(html.toString(), ContentType.Text.Html)
            }
            get("/logs/{count}") {
                connection.sendIqRequestAsync(GetLogsPacket(user, bot, call.parameters["count"]!!))
            }
            get("/progress/{activity}") {
                connection.sendIqRequestAsync(GetProgressAction(user, bot, call.parameters["activity"]!!))
            }
            get("/pullm/{mapDetailId}") {
                // <iq xmlns='jabber:client' to='username23@ecouser.net/resource' from='e0001057017609483161@115.ecorobot.net/atom' id='0F35BE4EC7B0' type='set'><query xmlns='com:ctl'><ctl id='pF2te-99' ret='ok' msid='7' tp='sa'><m mid='0' p='1'/><m mid='1' p='1'/><m mid='2' p='1'/><m mid='3' p='1'/><m mid='4' p='1'/><m mid='5' p='1'/></ctl></query></iq>
                connection.sendIqRequestAsync(PullMPacket(user, bot, MapSetType.SpotArea, 7, call.parameters["mapDetailId"]!!.toInt(), 0))
            }
            get("/GetSched") {
                connection.sendIqRequestAsync(GetSchedPacket(user, bot))
            }
        }
    }.start(wait = true)

    connection.disconnect()
}


