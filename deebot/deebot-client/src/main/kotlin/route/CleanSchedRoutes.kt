/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vacuumclean.deebot.client.route

import de.caterdev.vacuumclean.deebot.client.DeebotClient
import de.caterdev.vacuumclean.deebot.core.clean.CleanSchedule
import de.caterdev.vacuumclean.deebot.core.constants.Day
import io.ktor.application.call
import io.ktor.freemarker.FreeMarkerContent
import io.ktor.request.receiveParameters
import io.ktor.response.respond
import io.ktor.response.respondRedirect
import io.ktor.routing.Route
import io.ktor.routing.get
import io.ktor.routing.post

fun Route.cleanSched(client: DeebotClient) {
    get("/cleanSched/list") {
        val cleanSchedules = client.getCleanSchedules()
        val values = mapOf("cleanScheds" to cleanSchedules)

        call.respond(FreeMarkerContent("sched/list_schedules.html", values))
    }
    get("/cleanSched/add") {
        val values = mapOf("cleanSchedule" to CleanSchedule(), "days" to Day.values())

        call.respond(FreeMarkerContent("sched/edit_schedule.html", values))
    }
    get("/cleanSched/{scheduleId}/edit") {
        val values = mapOf("cleanSchedule" to client.cleanSchedules[call.parameters["scheduleId"]!!], "days" to Day.values())

        call.respond(FreeMarkerContent("sched/edit_schedule.html", values))
    }
    post("/cleanSched/update") {
        val parameters = call.receiveParameters()

        val days = StringBuilder()
        if (null != parameters["days1"]) {
            days.append("1")
        } else {
            days.append("0")
        }
        if (null != parameters["days2"]) {
            days.append("1")
        } else {
            days.append("0")
        }
        if (null != parameters["days3"]) {
            days.append("1")
        } else {
            days.append("0")
        }
        if (null != parameters["days4"]) {
            days.append("1")
        } else {
            days.append("0")
        }
        if (null != parameters["days5"]) {
            days.append("1")
        } else {
            days.append("0")
        }
        if (null != parameters["days6"]) {
            days.append("1")
        } else {
            days.append("0")
        }
        if (null != parameters["days7"]) {
            days.append("1")
        } else {
            days.append("0")
        }

        val cleanSchedule = CleanSchedule(parameters["name"], parameters["active"], parameters["hour"], parameters["minute"], days.toString(), "auto", "p")
        client.updateCleanSchedule(cleanSchedule)

        call.respondRedirect("/cleanSched/list")
    }
    get("/cleanSched/{scheduleId}/delete") {
        client.deleteCleanSchedule(call.parameters["scheduleId"]!!)

        call.respondRedirect("/cleanSched/list")
    }
}