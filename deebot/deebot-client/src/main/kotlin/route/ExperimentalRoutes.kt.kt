/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vacuumclean.deebot.client.route

import de.caterdev.vaccumclean.deebot.smack.packets.action.SimpleActionPacket
import de.caterdev.vaccumclean.deebot.smack.packets.action.admin.*
import de.caterdev.vaccumclean.deebot.smack.packets.action.map.GetMapBuildStatePacket
import de.caterdev.vaccumclean.deebot.smack.packets.action.map.IfHaveMapPacket
import de.caterdev.vaccumclean.deebot.smack.packets.action.map.RecoverMapPacket
import de.caterdev.vacuumclean.deebot.client.DeebotClient
import io.ktor.application.call
import io.ktor.routing.Route
import io.ktor.routing.get

fun Route.experimental(client: DeebotClient) {
    get("/getMapBuildState") {
        client.iqManager.sendPacket(GetMapBuildStatePacket(client.user, client.activeRobot!!.jid))
    }
    get("/getResource") {
        client.iqManager.sendPacket(GetResourcePacket(client.user, client.activeRobot!!.jid))
    }
    get("/ifHaveMap") {
        client.iqManager.sendPacket(IfHaveMapPacket(client.user, client.activeRobot!!.jid))
    }
    get("/recoverMap") {
        client.iqManager.sendPacket(RecoverMapPacket(client.user, client.activeRobot!!.jid))
    }
    get("/getNetInfo") {
        client.iqManager.sendPacket(GetNetInfoPacket(client.user, client.activeRobot!!.jid))
    }
    get("/getNetInfoFromFW") {
        client.iqManager.sendPacket(GetNetInfoFromFWPacket(client.user, client.activeRobot!!.jid))
    }
    get("/getSysInfo") {
        client.iqManager.sendPacket(GetSysInfoPacket(client.user, client.activeRobot!!.jid))
    }
    get("/getUARTInfo") {
        client.iqManager.sendPacket(GetUARTInfoPacket(client.user, client.activeRobot!!.jid))
    }
    get("/generic/{action}") {
        client.iqManager.sendPacket(SimpleActionPacket(client.user, client.activeRobot!!.jid, call.parameters["action"]))
    }
}