/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vacuumclean.deebot.client

import de.caterdev.vaccumclean.deebot.smack.packets.Query
import de.caterdev.vaccumclean.deebot.smack.packets.action.*
import de.caterdev.vaccumclean.deebot.smack.packets.action.clean.GetCleanLogsPacket
import de.caterdev.vaccumclean.deebot.smack.packets.action.clean.GetCleanSumPacket
import de.caterdev.vaccumclean.deebot.smack.packets.response.ErrorResponsePacket
import de.caterdev.vaccumclean.deebot.smack.packets.response.GetChargeStateResponsePacket
import de.caterdev.vaccumclean.deebot.smack.packets.response.ResponsePacket
import de.caterdev.vaccumclean.deebot.smack.packets.response.SimpleResponsePacket
import de.caterdev.vaccumclean.deebot.smack.packets.response.clean.GetCleanSumResponsePacket
import de.caterdev.vacuumclean.deebot.core.UserInfo
import de.caterdev.vacuumclean.deebot.core.clean.CleanStatistics
import de.caterdev.vacuumclean.deebot.core.map.MapInfo
import de.caterdev.vacuumclean.deebot.core.map.Position
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import org.jivesoftware.smack.XMPPConnection
import org.jivesoftware.smack.roster.Roster
import org.jxmpp.jid.Jid
import org.jxmpp.jid.impl.JidCreate
import java.time.Instant
import java.time.temporal.ChronoUnit
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

class Deebot930IQManager(val controls: MutableMap<String, ActionPacket<in Query>>, val connection: XMPPConnection, val robot: Bot) {
    suspend fun getChargeState(from: Jid, to: Jid): GetChargeStateResponsePacket.ChargeType {
        val packet = GetChargeStatePacket(from, to)
        controls.put(packet.innerId, packet as ActionPacket<in Query>)

        return suspendCoroutine { continuation ->
            packet.actionResponseListener = ActionResponseListener { responsePacket ->
                continuation.resume(responsePacket.chargeType)
            }

            connection.sendIqRequestAsync(packet)
        }
    }

    suspend fun getCleanLogs(from: Jid, to: Jid, count: String): List<CleanStatistics> {
        val packet = GetCleanLogsPacket(from, to, count)
        controls[packet.innerId] = packet as ActionPacket<in Query>

        return suspendCoroutine { continuation ->
            packet.actionResponseListener = ActionResponseListener { responsePacket ->
                continuation.resume(responsePacket.cleanStatistics)
            }
            connection.sendIqRequestAsync(packet)
        }
    }

    suspend fun getCleanSum(from: Jid, to: Jid): GetCleanSumResponsePacket {
        val packet = GetCleanSumPacket(from, to)
        controls[packet.innerId] = packet as ActionPacket<in Query>

        return suspendCoroutine { continuation ->
            packet.actionResponseListener = ActionResponseListener { responsePacket ->
                continuation.resume(responsePacket)
            }
            connection.sendIqRequestAsync(packet)
        }
    }

    suspend fun getTime(from: Jid, to: Jid): String {
        val packet = GetTimePacket(from, to)
        controls.put(packet.innerId, packet as ActionPacket<in Query>)

        return suspendCoroutine { continuation ->
            packet.actionResponseListener = ActionResponseListener { responsePacket ->
                continuation.resume(Instant.ofEpochSecond(responsePacket.time).plus(responsePacket.timeZone, ChronoUnit.HOURS).toString())
            }

            connection.sendIqRequestAsync(packet)
        }
    }

    suspend fun restoreMap(from: Jid, to: Jid): String {
        val packet = GetMapModelPacket(from, to)
        controls[packet.innerId] = packet as ActionPacket<in Query>

        return suspendCoroutine { continuation ->
            packet.actionResponseListener = ActionResponseListener { responsePacket ->
                robot.cleanMap.mapInfo = MapInfo(responsePacket.mapModel)

                GlobalScope.launch {
                    for (i in 0 until responsePacket.mapModel.crc.size) {
                        pullMP(from, to, i)
                    }
                    robot.cleanMap.updateScale(800f, 600f)
                }

                continuation.resume(responsePacket.mapModel.toString() + ", updated: " + robot.cleanMap.mapInfo + ", " + robot.cleanMap)
            }

            connection.sendIqRequestAsync(packet)
        }
    }

    suspend fun getMapModel(from: Jid, to: Jid): String {
        val packet = GetMapModelPacket(from, to)
        controls[packet.innerId] = packet as ActionPacket<in Query>

        return suspendCoroutine { continuation ->
            packet.actionResponseListener = ActionResponseListener { responsePacket ->
                robot.cleanMap.mapInfo = MapInfo(responsePacket.mapModel)
                robot.cleanMap.updateScale(800f, 600f)
                continuation.resume(responsePacket.mapModel.toString() + ", updated: " + robot.cleanMap.mapInfo + ", " + robot.cleanMap)
            }

            connection.sendIqRequestAsync(packet)
        }
    }

    suspend fun compareChecksums(from: Jid, to: Jid): String {
        val packet = GetMapModelPacket(from, to)
        controls[packet.innerId] = packet as ActionPacket<in Query>

        return suspendCoroutine { continuation ->
            packet.actionResponseListener = ActionResponseListener { responsePacket ->
                val mapModel = responsePacket.mapModel

                val result = StringBuilder()
                for (i in 0 until mapModel.crc.size) {
                    result.append(i).append(": ").append(mapModel.crc[i]).append(" - ").append(robot.cleanMap.mapInfo.getChecksum(i)).append("\n")
                    if (!mapModel.crc[i].equals(robot.cleanMap.mapInfo.getChecksum(i))) {
                        result.append("\twarning: checksums do not match!")
                    }
                }

                continuation.resume(result.toString())
            }

            connection.sendIqRequestAsync(packet)
        }
    }

    suspend fun setMapAutoReport(from: Jid, to: Jid, report: String): ResponsePacket.RetValue {
        val packet = SetMapAutoReportPacket(from, to, report)
        controls.put(packet.innerId, packet as ActionPacket<in Query>)

        return suspendCoroutine { continuation ->
            packet.actionResponseListener = ActionResponseListener { responsePacket ->
                continuation.resume(responsePacket.retValue)
            }

            connection.sendIqRequestAsync(packet)
        }
    }

    suspend fun charge(from: Jid, to: Jid, chargeAction: ChargePacket.ChargeAction): ResponsePacket.RetValue {
        val packet = ChargePacket(from, to, chargeAction)
        controls[packet.innerId] = packet as ActionPacket<in Query>

        return suspendCoroutine { continuation ->
            packet.actionResponseListener = object : ActionResponseListener<SimpleResponsePacket> {
                override fun onResponse(responsePacket: SimpleResponsePacket) = continuation.resume(responsePacket.retValue)
                override fun onError(responsePacket: ErrorResponsePacket) = continuation.resume(ResponsePacket.RetValue.fail)
            }

            connection.sendIqRequestAsync(packet)
        }
    }

    suspend fun getBatteryInfo(from: Jid, to: Jid): String {
        val packet = GetBatteryInfoPacket(from, to)
        controls.put(packet.innerId, packet as ActionPacket<in Query>)

        return suspendCoroutine { continuation ->
            packet.actionResponseListener = ActionResponseListener { responsePacket ->
                continuation.resume(responsePacket.power)
            }

            connection.sendIqRequestAsync(packet)
        }
    }

    suspend fun getUserInfo(from: Jid, to: Jid): Collection<UserInfo> {
        val packet = GetUserInfoPacket(from, to)
        controls.put(packet.innerId, packet as ActionPacket<in Query>)

        return suspendCoroutine { continuation ->
            packet.actionResponseListener = ActionResponseListener { responsePacket ->
                continuation.resume(responsePacket.userInfos)
            }

            connection.sendIqRequestAsync(packet)
        }
    }

    suspend fun getChargerPos(from: Jid, to: Jid): Position {
        val packet = GetChargerPosPacket(from, to)
        controls.put(packet.innerId, packet as ActionPacket<in Query>)

        return suspendCoroutine { continuation ->
            packet.actionResponseListener = ActionResponseListener { responsePacket ->
                val pos = responsePacket.pos
                robot.cleanMap.chargerPosition = pos

                continuation.resume(pos)
            }

            connection.sendIqRequestAsync(packet)
        }
    }

    suspend fun pullMP(from: Jid, to: Jid, pieceId: Int): String {
        val packet = PullMapPiecePacket(from, to, pieceId)
        controls.put(packet.innerId, packet as ActionPacket<in Query>)

        return suspendCoroutine { continuation ->
            packet.actionResponseListener = ActionResponseListener { responsePacket ->
                responsePacket.mapPiece.pieceId = pieceId

                if (robot.cleanMap.mapInfo.getChecksum(pieceId) != responsePacket.mapPiece.checksum) {
                    robot.cleanMap.mapInfo.updateMapBuffer(responsePacket.mapPiece)
                }

                continuation.resume(responsePacket.mapPiece.toString())
            }

            connection.sendIqRequestAsync(packet)
        }
    }

    suspend fun getPos(from: Jid, to: Jid): Position {
        val packet = GetPosPacket(from, to)
        controls.put(packet.innerId, packet as ActionPacket<in Query>)

        return suspendCoroutine { continuation ->
            packet.actionResponseListener = ActionResponseListener { responsePacket ->
                val pos = responsePacket.pos
                robot.cleanMap.botPosition = pos

                continuation.resume(pos)
            }

            connection.sendIqRequestAsync(packet)
        }
    }

    suspend fun getTraceData(from: Jid, to: Jid, traceId: String, traceFrom: String, traceTo: String): String {
        println("getting trace data for " + traceId + " -- " + traceFrom + "-" + traceTo)

        val packet = GetTraceInfoPacket(from, to, traceId, traceFrom, traceTo)
        controls[packet.innerId] = packet as ActionPacket<in Query>

        return suspendCoroutine { continuation ->
            packet.actionResponseListener = ActionResponseListener { responsePacket ->
                continuation.resume(responsePacket.traceData)
            }

            connection.sendIqRequestAsync(packet)
        }
    }

    fun authoriseUserWithBot(): List<Jid> {
        val roster: Roster = Roster.getInstanceFor(connection)
        if (!roster.isLoaded) {
            roster.reloadAndWait()
        }


        val bots: MutableList<Jid> = ArrayList(roster.entries.size)
        roster.entries.forEach {
            // TODO: prüfen, dass das immer funktioniert
            val bot: Jid = JidCreate.from(it.jid.asUnescapedString() + "/" + it.groups[0].name)
            bots.add(bot)

            connection.sendIqRequestAsync(GetUserInfoPacket(connection.user, bot))
        }

        if (bots.size == 0) {
            bots.add(JidCreate.from("centralbot"))
        }

        return bots
    }
}