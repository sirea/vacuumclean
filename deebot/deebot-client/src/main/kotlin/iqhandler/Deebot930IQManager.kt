/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vacuumclean.deebot.client.iqhandler

import de.caterdev.vaccumclean.deebot.smack.packets.action.*
import de.caterdev.vaccumclean.deebot.smack.packets.action.admin.GetWaterBoxInfoPacket
import de.caterdev.vaccumclean.deebot.smack.packets.action.admin.GetWaterPermeabilityPacket
import de.caterdev.vaccumclean.deebot.smack.packets.action.admin.SetTimePacket
import de.caterdev.vaccumclean.deebot.smack.packets.action.clean.GetCleanLogsPacket
import de.caterdev.vaccumclean.deebot.smack.packets.action.clean.GetCleanSumPacket
import de.caterdev.vaccumclean.deebot.smack.packets.action.map.ClearMapPacket
import de.caterdev.vaccumclean.deebot.smack.packets.action.map.GetMapSetPacket
import de.caterdev.vaccumclean.deebot.smack.packets.action.map.PullMPacket
import de.caterdev.vaccumclean.deebot.smack.packets.action.map.RenameMapSetPacket
import de.caterdev.vaccumclean.deebot.smack.packets.action.sched.AddSchedPacket
import de.caterdev.vaccumclean.deebot.smack.packets.action.sched.DeleteSchedPacket
import de.caterdev.vaccumclean.deebot.smack.packets.action.sched.GetSchedPacket
import de.caterdev.vaccumclean.deebot.smack.packets.action.sched.ModSchedPacket
import de.caterdev.vaccumclean.deebot.smack.packets.response.*
import de.caterdev.vaccumclean.deebot.smack.packets.response.admin.GetWaterBoxInfoResponsePacket
import de.caterdev.vaccumclean.deebot.smack.packets.response.admin.GetWaterPermeabilityResponsePacket
import de.caterdev.vaccumclean.deebot.smack.packets.response.clean.GetCleanLogsResponsePacket
import de.caterdev.vaccumclean.deebot.smack.packets.response.clean.GetCleanSumResponsePacket
import de.caterdev.vaccumclean.deebot.smack.packets.response.map.GetMapSetResponsePacket
import de.caterdev.vaccumclean.deebot.smack.packets.response.map.PullMResponsePacket
import de.caterdev.vaccumclean.deebot.smack.packets.response.sched.GetSchedResponsePacket
import de.caterdev.vacuumclean.deebot.client.model.Bot
import de.caterdev.vacuumclean.deebot.core.clean.CleanSchedule
import de.caterdev.vacuumclean.deebot.core.map.MapSetType
import org.jivesoftware.smack.XMPPConnection
import org.jivesoftware.smack.roster.Roster
import org.jxmpp.jid.Jid
import java.util.*
import kotlin.collections.HashMap
import kotlin.coroutines.resume
import kotlin.coroutines.suspendCoroutine

class Deebot930IQManager(private val connection: XMPPConnection, private val sentPackets: MutableMap<String, ActionPacket<ResponsePacket>>) {
    fun getRoster(): Map<String, Bot> {
        val roster = Roster.getInstanceFor(connection)

        if (!roster.isLoaded) {
            roster.reloadAndWait()
        }

        val bots: MutableMap<String, Bot> = HashMap(roster.entries.size)
        roster.entries.forEach { entry ->
            bots[entry.name] = Bot(entry)
        }

        return bots
    }

    suspend fun getCleanSum(from: Jid, to: Jid): GetCleanSumResponsePacket {
        return sendPacket(GetCleanSumPacket(from, to)) as GetCleanSumResponsePacket
    }

    suspend fun getCleanLogs(from: Jid, to: Jid, count: Int): GetCleanLogsResponsePacket {
        return sendPacket(GetCleanLogsPacket(from, to, count.toString())) as GetCleanLogsResponsePacket
    }

    suspend fun getWaterPermeAbility(from: Jid, to: Jid): GetWaterPermeabilityResponsePacket {
        return sendPacket(GetWaterPermeabilityPacket(from, to)) as GetWaterPermeabilityResponsePacket
    }

    suspend fun getWaterBoxInfo(from: Jid, to: Jid): GetWaterBoxInfoResponsePacket {
        return sendPacket(GetWaterBoxInfoPacket(from, to)) as GetWaterBoxInfoResponsePacket
    }

    suspend fun setMapAutoReport(from: Jid, to: Jid, report: String): ResponsePacket {
        return sendPacket(SetMapAutoReportPacket(from, to, report))
    }

    suspend fun getMapModel(from: Jid, to: Jid): GetMapModelResponsePacket {
        return sendPacket(GetMapModelPacket(from, to)) as GetMapModelResponsePacket
    }

    suspend fun pullMapPiece(from: Jid, to: Jid, pieceId: Int): PullMapPieceResponsePacket {
        val response = sendPacket(PullMapPiecePacket(from, to, pieceId)) as PullMapPieceResponsePacket
        response.mapPiece.pieceId = pieceId

        return response
    }

    suspend fun getMapSetType(from: Jid, to: Jid, mapSetType: MapSetType): GetMapSetResponsePacket {
        return sendPacket(GetMapSetPacket(from, to, mapSetType)) as GetMapSetResponsePacket
    }

    suspend fun pullMapSet(from: Jid, to: Jid, mapSetType: MapSetType, mapSetId: Int, mapDetailId: Int, seq: Int): PullMResponsePacket {
        return sendPacket(PullMPacket(from, to, mapSetType, mapSetId, mapDetailId, seq)) as PullMResponsePacket
    }

    suspend fun getTraceModel(from: Jid, to: Jid): GetTraceModelResponsePacket {
        return sendPacket(GetTraceModelPacket(from, to)) as GetTraceModelResponsePacket
    }

    suspend fun getTraceInfo(from: Jid, to: Jid, traceId: String, traceFrom: String, traceTo: String): GetTraceInfoResponsePacket {
        return sendPacket(GetTraceInfoPacket(from, to, traceId, traceFrom, traceTo)) as GetTraceInfoResponsePacket
    }

    suspend fun getTime(from: Jid, to: Jid): GetTimeResponsePacket {
        return sendPacket(GetTimePacket(from, to)) as GetTimeResponsePacket
    }

    suspend fun addCleanSchedule(from: Jid, to: Jid, cleanSchedule: CleanSchedule): ResponsePacket {
        return sendPacket(AddSchedPacket(from, to, cleanSchedule))
    }

    suspend fun modCleanSchedule(from: Jid, to: Jid, cleanSchedule: CleanSchedule): ResponsePacket {
        return sendPacket(ModSchedPacket(from, to, cleanSchedule.name, cleanSchedule))
    }

    suspend fun deleteCleanSchedule(from: Jid, to: Jid, cleanScheduleName: String): ResponsePacket {
        return sendPacket(DeleteSchedPacket(from, to, cleanScheduleName))
    }

    suspend fun clearMap(from: Jid, to: Jid): ResponsePacket {
        return sendPacket(ClearMapPacket(from, to))
    }

    suspend fun getCleanSchedules(from: Jid, to: Jid): GetSchedResponsePacket {
        val responsePacket = sendPacket(GetSchedPacket(from, to))

        if (responsePacket is SimpleResponsePacket) {
            return GetSchedResponsePacket(responsePacket)
        } else {
            return responsePacket as GetSchedResponsePacket
        }
    }

    suspend fun charge(from: Jid, to: Jid): ResponsePacket {
        return sendPacket(ChargePacket(from, to, ChargePacket.ChargeAction.go))
    }

    suspend fun clean(from: Jid, to: Jid, type: String, speed: String, act: String, mids: Array<String>?): ResponsePacket {
        val midBuilder = StringBuilder()
        if (null != mids) {
            midBuilder.append(mids.joinToString { s -> s })
        }


        return sendPacket(CleanPacket(from, to, type, speed, act, midBuilder.toString()))
    }

    suspend fun renameSpotAreas(from: Jid, to: Jid, mapSetType: MapSetType, mapSetId: Int, mapDetailNames: Map<String, String>): ResponsePacket {
        return sendPacket(RenameMapSetPacket(from, to, mapSetType, mapSetId.toString(), mapDetailNames))
    }

    suspend fun updateTime(from: Jid, to: Jid): ResponsePacket {
        val calendar = Calendar.getInstance()
        val time = calendar.timeInMillis / 1000
        val timeZone = (calendar.timeZone.rawOffset + calendar[Calendar.DST_OFFSET]) / 3600000

        return sendPacket(SetTimePacket(from, to, time, timeZone))
    }

    /**
     * Sends the given [packet] using the existing XMPP [connection] and waits for a response.
     *
     * After the incoming response, the [ActionResponseListener] of the [packet] is called.
     */
    public suspend fun sendPacket(packet: ActionPacket<out ResponsePacket>): ResponsePacket {
        sentPackets[packet.innerId] = packet as ActionPacket<ResponsePacket>


        return suspendCoroutine { continuation ->
            packet.actionResponseListener = ActionResponseListener { responsePacket ->
                continuation.resume(responsePacket)
            }

            connection.sendIqRequestAsync(packet)
        }
    }
}