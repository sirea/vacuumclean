/*
 * BSD 3-Clause License
 *
 * Copyright (c) 2019, Michael Becker
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the following disclaimer.
 *
 *  Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the following disclaimer in the documentation
 *   and/or other materials provided with the distribution.
 *
 *  Neither the name of the copyright holder nor the names of its
 *   contributors may be used to endorse or promote products derived from
 *   this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 */

package de.caterdev.vacuumclean.deebot.client.iqhandler

import de.caterdev.vaccumclean.deebot.smack.packets.Query
import de.caterdev.vaccumclean.deebot.smack.packets.action.ActionPacket
import de.caterdev.vaccumclean.deebot.smack.packets.action.AddUserPacket
import de.caterdev.vaccumclean.deebot.smack.packets.action.SetACPacket
import de.caterdev.vaccumclean.deebot.smack.packets.info.InfoPacket
import de.caterdev.vaccumclean.deebot.smack.packets.info.MapPInfoPacket
import de.caterdev.vaccumclean.deebot.smack.packets.info.PosInfoPacket
import de.caterdev.vaccumclean.deebot.smack.packets.info.TraceInfoPacket
import de.caterdev.vaccumclean.deebot.smack.packets.response.ErrorResponsePacket
import de.caterdev.vaccumclean.deebot.smack.packets.response.ResponsePacket
import de.caterdev.vacuumclean.deebot.client.model.Bot
import de.caterdev.vacuumclean.deebot.core.DataParseUtils
import de.caterdev.vacuumclean.deebot.core.ErrorNumbers
import org.jivesoftware.smack.XMPPConnection
import org.jivesoftware.smack.iqrequest.AbstractIqRequestHandler
import org.jivesoftware.smack.iqrequest.IQRequestHandler
import org.jivesoftware.smack.packet.IQ
import org.jxmpp.jid.impl.JidCreate

class Deebot930IQRequestHandler(private val connection: XMPPConnection, private val sentPackets: MutableMap<String, ActionPacket<ResponsePacket>>, private val activeRobot: Bot) : AbstractIqRequestHandler(Query.ELEMENT, Query.NAMESPACE, IQ.Type.set, IQRequestHandler.Mode.async) {
    override fun handleIQRequest(iqRequest: IQ): IQ? {
        if (iqRequest is ResponsePacket) {
            val control = sentPackets.getValue(iqRequest.innerId)

            when (iqRequest) {
                is ErrorResponsePacket -> {

                    if (iqRequest.errno.toInt() == ErrorNumbers.PermissionDenied.errno) {
                        // make the current user admin
                        val adminUser = iqRequest.errorMessage.replace("permission denied, please contact ", "").trim();
                        println(adminUser)

                        connection.fromMode = XMPPConnection.FromMode.UNCHANGED
                        val addUserPacket = AddUserPacket(JidCreate.from(adminUser), iqRequest.from, iqRequest.to)
                        val setACPacket = SetACPacket(JidCreate.from(adminUser), iqRequest.from, iqRequest.to, true, true, true)

                        //connection.sendStanza(addUserPacket)
                        connection.sendIqRequestAndWaitForResponse<ResponsePacket>(addUserPacket)

                        return setACPacket
                    } else {
                        control.actionResponseListener.onError(iqRequest)
                    }
                }
                else -> {
                    control.actionResponseListener.onResponse(iqRequest)
                    sentPackets.remove(iqRequest.innerId)
                }
            }
        } else if (iqRequest is InfoPacket) {
            when (iqRequest) {
                is TraceInfoPacket -> {
                    if (!activeRobot.mappedTraces.containsKey(iqRequest.traceId)) {
                        activeRobot.mappedTraces[iqRequest.traceId] = ArrayList<String>()
                    }
                    activeRobot.mappedTraces[iqRequest.traceId]!!.add(iqRequest.toXML(null).toString())

                    println("trace: " + DataParseUtils.parseTracePoints(iqRequest.tr))
                }
                is PosInfoPacket -> {
                    println("position: " + iqRequest.pos)
                }
                is MapPInfoPacket -> {
                    val mapP = iqRequest.mapPiece
                    val mapInfo = activeRobot.cleanMap.mapInfo

                    if (0L != mapInfo.getChecksum(mapP.pieceId) && mapInfo.getChecksum(mapP.pieceId) != mapP.checksum) {
                        mapInfo.updateMapBuffer(mapP)
                    }
                    activeRobot.cleanMap.updateScale(800f, 600f)
                    println(activeRobot.cleanMap)
                    val rects = activeRobot.cleanMap.update()
                }
            }
        }

        return null
    }
}
